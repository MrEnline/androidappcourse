package ua.kh.em.auiconstraint.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ua.kh.em.auiconstraint.R;

public class ConstraintActivity extends AppCompatActivity {

    final Context context = this;
    Button buttonEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint);

        buttonEnter = findViewById(R.id.bt_enter);

        buttonEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.toast_enter,Toast.LENGTH_LONG).show();
            }
        });
    }
}
