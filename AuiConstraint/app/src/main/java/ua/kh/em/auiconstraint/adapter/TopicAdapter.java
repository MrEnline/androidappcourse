package ua.kh.em.auiconstraint.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ua.kh.em.auiconstraint.R;
import ua.kh.em.auiconstraint.activity.ConstraintActivity;
import ua.kh.em.auiconstraint.activity.BaselineActivity;
import ua.kh.em.auiconstraint.activity.GuideLineActivity;
import ua.kh.em.auiconstraint.activity.BarrierActivity;
import ua.kh.em.auiconstraint.activity.MainActivity;
import ua.kh.em.auiconstraint.activity.RatioActivity;
import ua.kh.em.auiconstraint.activity.ChainActivity;
import ua.kh.em.auiconstraint.activity.SampleActivity;
import ua.kh.em.auiconstraint.model.Topic;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.ViewHolder> {

    private List<Topic> topicList;
    private Context context;

    public TopicAdapter(List<Topic> topicList, Context context) {
        this.topicList = topicList;
        this.context = context;
    }

    @NonNull
    @Override
    public TopicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TopicAdapter.ViewHolder holder, int position) {
        holder.itemTopic.setText(topicList.get(position).getTopic());

        holder.itemTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (holder.getAdapterPosition()){
                    case 0:
                        context.startActivity(new Intent(context, ConstraintActivity.class));
                        break;
                    case 1:
                        context.startActivity(new Intent(context, BaselineActivity.class));
                        break;
                    case 2:
                        context.startActivity(new Intent(context, GuideLineActivity.class));
                        break;
                    case 3:
                        context.startActivity(new Intent(context, BarrierActivity.class));
                        break;
                    case 4:
                        context.startActivity(new Intent(context, RatioActivity.class));
                        break;
                    case 5:
                        context.startActivity(new Intent(context, ChainActivity.class));
                        break;
                    case 6:
                        context.startActivity(new Intent(context, SampleActivity.class));
                        break;
                    default:
                        context.startActivity(new Intent(context, MainActivity.class));
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return topicList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemTopic;
        public ViewHolder(View itemView) {
            super(itemView);
            itemTopic = itemView.findViewById(R.id.item_topic);
        }
    }
}
