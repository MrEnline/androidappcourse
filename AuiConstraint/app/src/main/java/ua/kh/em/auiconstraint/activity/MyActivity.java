package ua.kh.em.auiconstraint.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ua.kh.em.auiconstraint.R;

public class MyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }
}
