package ua.kh.em.auiconstraint.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ua.kh.em.auiconstraint.R;

public class SampleActivity extends AppCompatActivity {

    final Context context = this;
    Button buttonMessage;
    Button buttonOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        buttonMessage = findViewById(R.id.button_message);
        buttonOrder = findViewById(R.id.button_order);

        buttonMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.toast_message,Toast.LENGTH_LONG).show();
            }
        });

        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.toast_order,Toast.LENGTH_LONG).show();
            }
        });
    }
}
