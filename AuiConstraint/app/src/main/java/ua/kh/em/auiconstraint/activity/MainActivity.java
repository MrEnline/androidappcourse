package ua.kh.em.auiconstraint.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ua.kh.em.auiconstraint.R;
import ua.kh.em.auiconstraint.adapter.TopicAdapter;
import ua.kh.em.auiconstraint.model.Topic;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    private String [] topics;
    private List<Topic> topicList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topics = getResources().getStringArray(R.array.topics_array);

        topicList = new ArrayList<>();

        for (String topicz : topics){
            Topic tops = new Topic(topicz);
            topicList.add(tops);
        }

        recyclerView = findViewById(R.id.topic_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        // разделитель между элементами RecyclerView
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        TopicAdapter topicAdapter = new TopicAdapter(topicList,context);
        recyclerView.setAdapter(topicAdapter);
    }
}
