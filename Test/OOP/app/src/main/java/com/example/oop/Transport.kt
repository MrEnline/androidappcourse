package com.example.oop

abstract class Transport(open val name: String) {
    abstract fun drive(): String
}