package com.example.oop

interface WaterCarrier {
    fun carryWater()
}