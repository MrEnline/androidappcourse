package com.example.oop

class MyRandom {
    companion object{
        fun randomNumber(from: Int, to: Int): Int = Math.random()
    }
}