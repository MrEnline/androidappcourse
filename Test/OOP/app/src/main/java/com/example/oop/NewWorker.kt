package com.example.oop

open class NewWorker(val name: String, val age: Int) {
    open fun work() = "Работаю"
}