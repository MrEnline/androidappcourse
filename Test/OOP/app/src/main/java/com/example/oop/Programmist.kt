package com.example.oop

class Programmist(name: String, age: Int, val langProgrammist: String): NewWorker(name, age) {
    override fun work() = "Пишу на " + langProgrammist
}