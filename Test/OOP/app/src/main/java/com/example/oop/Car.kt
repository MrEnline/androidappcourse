package com.example.oop

class Car(override var name: String): Transport(name) {
    val isStartEngine: Boolean = false

    override fun drive() = "Ехать"

    fun startEngine() = isStartEngine

}