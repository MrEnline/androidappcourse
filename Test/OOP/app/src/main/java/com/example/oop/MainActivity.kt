package com.example.oop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val dog = Dog ()
//        dog.age = 11
//        dog.weight = 25
//        dog.nickName = "vasya"
//
//        Log.i("DOG", dog.nickName!!)

//        val book = Book("Tili Tili Test", "2021", 876.02f)
//        book.yearEdition = "2020"
//        book.price = 1234f

//        val worker = Worker("Vasya", "programmist", 2020)
//        Log.i("Worker", worker.name + " " + worker.post + " " + worker.isExperience)
//        fun Worker.isAllInfo() = "Имя: " + worker.name + " Профессия:" + worker.post + " Стаж:" + worker.isExperience + " лет"
//        Log.i("Worker", worker.isAllInfo())

//        val listWorker = listOf(Programmist("Vasya", 25, "Kotlin"),
//                                NewWorker("Petya", 30),
//                                Programmist("Kolya", 35, "Java"),
//                                NewWorker("Olya", 28))
//
//        for (worker in listWorker){
//            Log.i("Worker", worker.name + " " +  worker.work())
//        }

        val sportsmen = Sportsmen("Vasya")
        sportsmen.callWaterCarrier(object: WaterCarrier {
            override fun carryWater() {
                TODO("Not yet implemented")
            }
        })

    }

}