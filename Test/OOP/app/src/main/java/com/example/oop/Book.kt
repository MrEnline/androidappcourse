package com.example.oop

import java.util.*

class Book(val name: String, var yearEdition: String? = null, var price: Float? = null)