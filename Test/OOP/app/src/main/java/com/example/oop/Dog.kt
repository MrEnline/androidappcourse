package com.example.oop

class Dog {
    var nickName: String = ""

    get() {return field.lowercase().capitalize() }

    set(value){
        field = value
    }

    var weight: Int = 0
        get() = weight

    var age: Int = 0
        get() = age
        set(value) {
            if (value >= 0)
                field = value
            else
                field = 0
        }
}