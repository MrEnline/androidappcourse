package com.example.oop

class Bicycle(override var name: String): Transport(name) {
    override fun drive() = "Ехать"
}