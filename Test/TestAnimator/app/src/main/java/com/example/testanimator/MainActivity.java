package com.example.testanimator;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPropertyAnimator viewPropertyAnimatorDown;
    ViewPropertyAnimator viewPropertyAnimatorUp;
    AnimatorSet animatorSet;
    ImageView imageViewCircle;
    EditText editTextUp;
    EditText editTextDown;
    EditText editTextPause1;
    EditText editTextPause2;
    private static boolean isAnimate = false;
    private static boolean isAnimateDown = false;
    private static boolean isChangeTime = false;
    private static boolean isStop = false;
    private static long DELAY_INHALE = 2000;
    private static long DELAY_EXHALE = 2000;
    private static long TIME_INHALE = 4000;
    private static long TIME_EXHALE = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageViewCircle = findViewById(R.id.imageViewCircle);
        editTextUp = findViewById(R.id.editTextUp);
        editTextDown = findViewById(R.id.editTextDown);
        editTextPause1 = findViewById(R.id.editTextPause1);
        editTextPause2 = findViewById(R.id.editTextPause2);
        editTextUp.setText(String.valueOf(TIME_INHALE));
        editTextDown.setText(String.valueOf(TIME_EXHALE));
        editTextPause1.setText(String.valueOf(DELAY_INHALE));
        editTextPause2.setText(String.valueOf(DELAY_EXHALE));
//        viewPropertyAnimatorUp = imageViewCircle.animate().scaleXBy(1).scaleX(2).scaleYBy(1).scaleY(2);
//        viewPropertyAnimatorUp.setDuration(5000);
//        viewPropertyAnimatorUp.setStartDelay(3000);
//        viewPropertyAnimatorUp.setListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//                Log.i("TagUp", "TagUp");
//                viewPropertyAnimatorDown = imageViewCircle.animate().scaleXBy(1).scaleX(0.5f).scaleYBy(1).scaleY(0.5f);
//                viewPropertyAnimatorDown.setDuration(5000);
//                viewPropertyAnimatorDown.setStartDelay(3000);
//                viewPropertyAnimatorDown.setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        super.onAnimationEnd(animation);
//                        Log.i("TagDown", "Down");
//                        viewPropertyAnimatorUp = imageViewCircle.animate().scaleXBy(1).scaleX(2).scaleYBy(1).scaleY(2);
//                        viewPropertyAnimatorUp.setDuration(5000);
//                        viewPropertyAnimatorUp.setStartDelay(3000);
//                    }
//                });
//            }
//        });

//        if (imageViewCircle != null && !isAnimate){
//            animatorSet.playSequentially(increaseAnimatorSet(imageViewCircle), decreaseAnimatorSet(imageViewCircle));
//            animatorSet.addListener(getEndAnimatorListenerImageView(imageViewCircle,animatorSet));
//            animatorSet.start();
//                startAnimator(animatorSet, imageViewCircle);
//        }
        editTextUp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                EditText editText = (EditText) view;
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    TIME_INHALE = Integer.valueOf(editText.getText().toString());
                    isChangeTime = true;
                    return true;
                }
                return false;
            }
        });

        editTextDown.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                EditText editText = (EditText) view;
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    TIME_EXHALE = Integer.valueOf(editText.getText().toString());
                    isChangeTime = true;
                    return true;
                }
                return false;
            }
        });

        editTextPause1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                EditText editText = (EditText) view;
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    DELAY_EXHALE = Integer.valueOf(editText.getText().toString());
                    isChangeTime = true;
                    return true;
                }
                return false;
            }
        });

        editTextPause2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                EditText editText = (EditText) view;
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    DELAY_INHALE = Integer.valueOf(editText.getText().toString());
                    isChangeTime = true;
                    return true;
                }
                return false;
            }
        });
    }

    private static void startAnimator(AnimatorSet animatorSet, View view){
        if (animatorSet != null)
            animatorSet.removeAllListeners();
        if (animatorSet == null || isChangeTime) {
            animatorSet = new AnimatorSet();
            isChangeTime = false;
        }
        if (isStop){
            isStop = false;
            if (animatorSet != null)
                animatorSet = null;
            return;
        }
        animatorSet.playSequentially(increaseAnimatorSet(view), decreaseAnimatorSet(view));
        animatorSet.addListener(getEndAnimatorListenerImageView(view,animatorSet));
        animatorSet.start();
    }

    public void onStartStopAnimation(View view) {
        if (isAnimate) {
            isStop = true;
            return;
        }
        startAnimator(animatorSet, imageViewCircle);
    }

    private static AnimatorSet increaseAnimatorSet(View view) {
        AnimatorSet set = new AnimatorSet();
        set.setDuration(TIME_INHALE).playTogether(
                ObjectAnimator.ofFloat(view, View.SCALE_X, 1f, 2f),
                ObjectAnimator.ofFloat(view, View.SCALE_Y, 1f, 2f)
        );
        isAnimateDown = false;
        set.setStartDelay(DELAY_INHALE);
        return set;
    }

    private static AnimatorSet decreaseAnimatorSet(View view) {
        AnimatorSet set = new AnimatorSet();
        set.setDuration(TIME_EXHALE).playTogether(
                ObjectAnimator.ofFloat(view, View.SCALE_X, 2f, 1f),
                ObjectAnimator.ofFloat(view, View.SCALE_Y, 2f, 1f)
        );
        isAnimateDown = true;
        set.setStartDelay(DELAY_EXHALE);
        return set;
    }

    private static AnimatorListenerAdapter getEndAnimatorListenerImageView(final View view, AnimatorSet animatorSet){
        return new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                isAnimate = true;
                if (isStop)
                    animatorSet.cancel();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                isAnimate = false;
                if (isAnimateDown && !animation.isPaused()){
                    isAnimateDown = false;
                    startAnimator(animatorSet, view);
                }
            }
        };
    }
}