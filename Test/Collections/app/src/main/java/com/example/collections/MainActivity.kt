package com.example.collections

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val data = mapOf(
            "Январь" to listOf<Int>(100, 100, 100, 100),
            "Февраль" to listOf<Int>(200, 200, -190, 200),
            "Март" to listOf<Int>(300, 180, 300, 100),
            "Апрель" to listOf<Int>(250, -250, 100, 300),
            "Май" to listOf<Int>(200, 100, 400, 300),
            "Июнь" to listOf<Int>(200, 100, 300, 300)
        )

        printInfo(data)
    }

    fun printInfo(data: Map<String, List<Int>>){
        //средняя выручка в неделю
        //мой вариант
//        val averageList1 = data.flatMap { it.value }.map { it/4 }
//        for (i in averageList1){
//            Log.i("Tag", i.toString())
//        }
        val averageList = data.filterNot { it.value.any{it < 0} }
        val averageList1 = averageList.map { it.value }.flatMap { it }.average()

        Log.i("Tag", "Средняя выручка в неделю равна " + averageList1.toString())

        //средняя выручка в месяц
//        val averageList2 = data.map { it.value.average() }
//        val month1 = data.map { it.key }
//        val allArr = (data.map { it.key }).zip(data.map { it.value.average() })//month1.zip(averageList2)
//        for (item in allArr){
//            Log.i("Tag", item.first + " - " + item.second)
//        }
        val averageList2 = averageList1 * 4;
        Log.i("Tag", "Средняя выручка в месяц равна " + averageList2.toString())

        //минимальная и максимальная выручки в месяц и название месяца
//        val listMin = data.map{it.value.minOrNull()}
//        val listMax = data.map{it.value.maxOrNull()}
//        val listMonth = data.map{it.key}.zip((data.map{it.value.minOrNull()}).zip(data.map{it.value.maxOrNull()}))//data.map{it.key}.zip(listMin.zip(listMax))
//        for (item in listMonth){
//            Log.i("Tag", item.first + ": max - " + item.second.first + ". min - " + item.second.second)
//        }

        val list = averageList.map { it.value.sum() }
        val minMonth = list.minOrNull()
        val maxMonth = list.maxOrNull()
        val minMonthList = averageList.filter{it.value.sum() == minMonth}.keys
        val maxMonthList = averageList.filter { it.value.sum() == maxMonth }.keys;

        //исключить месяцы, в которых есть отрицательная выручка
        val averageList4 = data.filter { it.value.any{it < 0} }
        val errorMonth = averageList4.keys
        //вывести месяцы в которых была отрицательная выручка

    }
}

