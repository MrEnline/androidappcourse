package com.example.myfavouritesmovies.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.myfavouritesmovies.model.AppRepository;
import com.example.myfavouritesmovies.model.Genre;
import com.example.myfavouritesmovies.model.Movie;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    AppRepository appRepository;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        appRepository = new AppRepository(application);
    }

    public LiveData<List<Genre>> getGenres(){
        return appRepository.getGenres();
    }

    public LiveData<List<Movie>> getGenreMovies(int genreId){
        return appRepository.getGenreMovies(genreId);
    }

    public void addNewMovie(Movie movie){
        appRepository.insertMovie(movie);
    }

    public void updateMovie(Movie movie){
        appRepository.updateMovie(movie);
    }

    public void deleteMovie(Movie movie){
        appRepository.deleteMovie(movie);
    }
}
