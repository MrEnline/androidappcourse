package com.example.myfavouritesmovies.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class AppRepository {
    private static GenreDao genreDao;
    private static MovieDao movieDao;
    private LiveData<List<Genre>> genres;
    private LiveData<List<Movie>> movies;

    public AppRepository(Application application) {
        MoviesDatabase moviesDatabase = MoviesDatabase.getInstance(application);
        genreDao = moviesDatabase.getGenreDao();
        movieDao = moviesDatabase.getMovieDao();
    }

    public LiveData<List<Genre>> getGenres(){
        return genreDao.getAllGenres();
    }

    public LiveData<List<Movie>> getGenreMovies(int genreId){
        return  movieDao.getGenreMovies(genreId);
    }

    public void insertGenre(Genre genre){
        new InsertGenreAsyncTask().execute(genre);
    }

    public void insertMovie(Movie movie){
        new InsertMovieAsyncTask().execute(movie);
    }

    public void updateGenre(Genre genre){
        new UpdateGenreAsyncTask().execute(genre);
    }

    public void updateMovie(Movie movie){
        new UpdateMovieAsyncTask().execute(movie);
    }

    public void deleteGenre(Genre genre){
        new DeleteGenreAsyncTask().execute(genre);
    }

    public void deleteMovie(Movie movie){
        new DeleteMovieAsyncTask().execute(movie);
    }

    private static class InsertGenreAsyncTask extends AsyncTask<Genre, Void, Void>{
        @Override
        protected Void doInBackground(Genre... genres) {
            genreDao.insert(genres[0]);
            return null;
        }
    }

    private static class InsertMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.insert(movies[0]);
            return null;
        }
    }

    private static class UpdateGenreAsyncTask extends AsyncTask<Genre, Void, Void>{
        @Override
        protected Void doInBackground(Genre... genres) {
            genreDao.update(genres[0]);
            return null;
        }
    }

    private static class UpdateMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.update(movies[0]);
            return null;
        }
    }

    private static class DeleteGenreAsyncTask extends AsyncTask<Genre, Void, Void>{
        @Override
        protected Void doInBackground(Genre... genres) {
            genreDao.delete(genres[0]);
            return null;
        }
    }

    private static class DeleteMovieAsyncTask extends AsyncTask<Movie, Void, Void>{
        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.delete(movies[0]);
            return null;
        }
    }
}
