package com.example.myfavouritesmovies;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myfavouritesmovies.databinding.ActivityAddEditBinding;
import com.example.myfavouritesmovies.model.Movie;

public class AddEditActivity extends AppCompatActivity {

    private Movie movie;
    public static final String MOVIE_ID = "movie_id";
    public static final String MOVIE_NAME = "movie_name";
    public static final String MOVIE_DESCRIPTION = "movie_description";
    private ActivityAddEditBinding activityAddEditBinding;
    private AddEditClickHandlers addEditClickHandlers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        movie = new Movie();
        activityAddEditBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_edit);
        activityAddEditBinding.setMovie(movie);
        addEditClickHandlers = new AddEditClickHandlers(this);
        activityAddEditBinding.setClickHandlers(addEditClickHandlers);
        Intent intent = getIntent();
        if (intent.hasExtra(MOVIE_ID)){
            setTitle("Edit movie");
            movie.setMovieName(intent.getStringExtra(MOVIE_NAME));
            movie.setMovieDescription(intent.getStringExtra(MOVIE_DESCRIPTION));
        }
        else{
            setTitle("Add movie");
        }
    }

    public class AddEditClickHandlers{
        Context context;

        public AddEditClickHandlers(Context context){
            this.context = context;
        }

        public void onOkButtonClick(View view){
            if (movie.getMovieName() == null){
                Toast.makeText(context, "Please input movie name", Toast.LENGTH_SHORT).show();
            }else{
                Intent intent = new Intent();
                intent.putExtra(MOVIE_NAME, movie.getMovieName());
                intent.putExtra(MOVIE_DESCRIPTION, movie.getMovieDescription());
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}