package com.example.mvvmretrofitdemo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mvvmretrofitdemo.R;
import com.example.mvvmretrofitdemo.databinding.ActivityMovieDetailsBinding;
import com.example.mvvmretrofitdemo.model.Result;

public class MovieDetailsActivity extends AppCompatActivity {
    Result result;
//    ImageView posterImageView;
//    String posterPath;
//    TextView titleTextView;
//    TextView voteCountTextView;
//    TextView overviewTextView;
    private ActivityMovieDetailsBinding activityMovieDetailsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        activityMovieDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);
        Intent intent = getIntent();
//        posterImageView = findViewById(R.id.imageView);
//        titleTextView = findViewById(R.id.titleTextView);
//        voteCountTextView = findViewById(R.id.voteCountTextView);
//        overviewTextView = findViewById(R.id.overviewTextView);
        if (intent != null && intent.hasExtra("movieData")){
            result = intent.getParcelableExtra("movieData");
//            Toast.makeText(this, result.getOriginalTitle(), Toast.LENGTH_LONG).show();
//            posterPath = result.getPosterPath();
//            String imagePath = "https://image.tmdb.org/t/p/w500/" + posterPath;
//            Glide.with(this).load(imagePath).placeholder(R.drawable.progress_circle).into(posterImageView);
//            titleTextView.setText(result.getOriginalTitle());
//            voteCountTextView.setText("Voices: " + Integer.toString(result.getVoteCount()));
//            overviewTextView.setText(result.getOverview());
            activityMovieDetailsBinding.setResult(result);
        }
    }
}