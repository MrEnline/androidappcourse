package com.example.databindingdemo3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import androidx.appcompat.widget.Toolbar;

import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.databindingdemo3.databinding.ActivityOkBinding;

public class OkActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityOkBinding activityOkBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        activityOkBinding = DataBindingUtil.setContentView(this, R.layout.activity_ok);
        activityOkBinding.setBook(getBook());
    }

    private Book getBook(){
        Book book = new Book();
        book.setTitle("Hamlet");
        book.setAuthor("Shakeskspear");
        return book;
    }
}