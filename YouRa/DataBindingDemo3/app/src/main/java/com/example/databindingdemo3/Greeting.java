package com.example.databindingdemo3;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class Greeting extends BaseObservable {

    private String senderName;
    private String greetingText;

    public Greeting(String senderName, String greetingText) {
        this.senderName = senderName;
        this.greetingText = greetingText;
    }

    @Bindable
    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
        notifyPropertyChanged(BR.senderName);
    }

    @Bindable
    public String getGreetingText() {
        return greetingText;
    }

    public void setGreetingText(String greetingText) {
        this.greetingText = greetingText;
        notifyPropertyChanged(BR.greetingText);
    }
}
