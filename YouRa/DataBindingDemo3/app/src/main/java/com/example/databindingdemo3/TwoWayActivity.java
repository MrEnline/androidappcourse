package com.example.databindingdemo3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.databindingdemo3.databinding.ActivityTwoWayBinding;

public class TwoWayActivity extends AppCompatActivity {

    private ActivityTwoWayBinding activityTwoWayBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_way);
        activityTwoWayBinding = DataBindingUtil.setContentView(this, R.layout.activity_two_way);
        activityTwoWayBinding.setGreeting(getGreeting());
    }

    private Greeting getGreeting(){
        return new Greeting("Vasya", "Hello");
    }
}