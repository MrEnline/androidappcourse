package com.example.databindingdemo3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.databindingdemo3.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

//    private TextView titleTextView;
//    private TextView authorTextView;

    private ActivityMainBinding activityMainBinding;
    private MainActivityButtonsHandler mainActivityButtonsHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        titleTextView = findViewById(R.id.titleTextView);
//        authorTextView = findViewById(R.id.authorTextView);
//        titleTextView.setText(getBook().getTitle());
//        authorTextView.setText(getBook().getAuthor());

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setBook(getBook());
        mainActivityButtonsHandler = new MainActivityButtonsHandler(this);
        activityMainBinding.setButtonHandler(mainActivityButtonsHandler);
    }

//    private void onOkClicked(){
//        Toast.makeText(this, "Ok", Toast.LENGTH_SHORT).show();
//    }
//
//    private void onCancelClicked(){
//        Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
//    }

    private Book getBook(){
        Book book = new Book();
        book.setTitle("Hamlet");
        book.setAuthor("Shakeskspear");
        return book;
    }

    public class MainActivityButtonsHandler{
        Context context;

        public MainActivityButtonsHandler(Context context) {
            this.context = context;
        }

        public void onOkClicked(View view){
            startActivity(new Intent(MainActivity.this, OkActivity.class));
        }

        public void onCancelClicked(View view){
            startActivity(new Intent(MainActivity.this, TwoWayActivity.class));
        }
    }
}