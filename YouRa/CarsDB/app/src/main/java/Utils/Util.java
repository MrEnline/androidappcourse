package Utils;

public class Util {
    //общая информация к БД
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "carsDB";
    public static final String TABLE_NAME = "cars";

    //столбцы БД
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PRICE = "price";
}
