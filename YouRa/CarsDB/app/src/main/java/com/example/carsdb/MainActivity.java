package com.example.carsdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import Data.DatabaseHandler;
import Model.Car;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHandler db = new DatabaseHandler(this);
/*        db.addCar(new Car(1, "Mazda", "30000 $"));
        db.addCar(new Car(1, "Opel", "40000 $"));
        db.addCar(new Car(1, "Kia", "20000 $"));
        db.addCar(new Car(1, "Toyota", "50000 $"));*/

        List<Car> carList = db.getAllCars();


        for (Car car: carList){
            Log.d("CarInfo", car.getId() + ", " + car.getName() + ", " + car.getPrice());
        }

/*        Car car = db.getCar(6);
        car.setName("Tesla");
        car.setPrice("60000 $");
        db.updateCar(car);
        Log.d("CarInfo", car.getId() + ", " + car.getName() + ", " + car.getPrice());*/

        //db.deleteCar(db.getCar(6));
        //db.deleteCar(db.getCar(5));
        for (Car car: carList){
            Log.d("CarInfo", car.getId() + ", " + car.getName() + ", " + car.getPrice());
        }
    }
}