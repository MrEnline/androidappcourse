package com.example.clubolymp.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.clubolymp.data.ClubOlimpContract.*;


public class OlympContentProvider extends ContentProvider {
    OlympDbOpenHelper dbOpenHelper;

    private static final int MEMBERS = 111;
    private static final int MEMBER_ID = 222;

    //класс UriMatcher позволяет работать с разными Uri
    //предварительно задав связав их с определенными значениями
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    //создаетм 2 вида URI в классе унаследованном от ContentProvider и присваиваем им значения заданные выше
    // MEMBERS и MEMBER_ID
    static{
        //присвоим значение для каждого Uri с помощью констант заданных выше
        //content://com.example.clubolymp/members
        uriMatcher.addURI(ClubOlimpContract.AUTHORITY, ClubOlimpContract.PATH_MEMBERS, MEMBERS);
        //content://com.example.clubolymp/members/34
        uriMatcher.addURI(ClubOlimpContract.AUTHORITY, ClubOlimpContract.PATH_MEMBERS + "/#", MEMBER_ID);
    }

    @Override
    public boolean onCreate() {
        dbOpenHelper = new OlympDbOpenHelper(getContext());
        return true;
    }

    //content://com.example.clubolymp/members - данный URI таблицы имеет свой код (в данном случае 111)
    //content://com.example.clubolymp/members/34 - данный URI столбца таблицы с id=34 имеет свой код (в данном случае 222)
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cursor;
        //UriMatcher преобразует полученный Uri в код, который задан через addUri.
        //Если Uri не совпадает с добавленными в блоке static, то возврат -1
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case MEMBERS:
                cursor = db.query(MemberEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case MEMBER_ID:
                //selection  = "=?"
                selection = MemberEntry._ID + "=?";
                //заносим в массив значение id из URI
                //selectionArgs = 34 - подставляется вместо знака вопроса
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))}; //вычленяем значение id из Uri
                cursor = db.query(MemberEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                Toast.makeText(getContext(), "IncorrectURI", Toast.LENGTH_LONG).show();
                throw  new IllegalArgumentException("Can't query incorrect URI " + uri);
        }
        //getContext().getContentResolver() подписывается на обновления(изменения uri)
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        //проверка правильности введенных данных для вставки в БД
        String firstName = contentValues.getAsString(MemberEntry.COLUMN_FIRST_NAME);
        if (firstName == null)
            throw new IllegalArgumentException("Uncorrect input first name");
        String lastName = contentValues.getAsString(MemberEntry.COLUMN_LAST_NAME);
        if (lastName == null)
            throw new IllegalArgumentException("Uncorrect input last name");
        Integer gender = contentValues.getAsInteger(MemberEntry.COLUMN_GENDER);
        if (gender == null || !(gender == MemberEntry.GENDER_UNKNOWN || gender == MemberEntry.GENDER_MALE || gender == MemberEntry.GENDER_FEMALE))
            throw new IllegalArgumentException("Uncorrect input gender");
        String sport = contentValues.getAsString(MemberEntry.COLUMN_SPORT);
        if (sport == null)
            throw new IllegalArgumentException("Uncorrect intput sport");

        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case MEMBERS:
                long id = db.insert(MemberEntry.TABLE_NAME, null, contentValues);
                if (id == -1){
                    Log.e("insertMethod", "Insertion of data in the table failed for " + uri);
                    return null;
                }
                //уведомляет того кто вызвал этот метод через ContentResolver о вставке элемента по uri
                getContext().getContentResolver().notifyChange(uri, null);
                //возвращает uri с вставленной строчкой в БД
                //content://com.example.clubolymp/members/34
                return ContentUris.withAppendedId(uri, id); //возвращаем Uri но уже с id добавленной строки
            default:
                throw  new IllegalArgumentException("Insertion of data in the table failed for " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int uriMatch = uriMatcher.match(uri);
        int rowsDeleted;
        switch (uriMatch){
            case MEMBERS:
                rowsDeleted = db.delete(MemberEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case MEMBER_ID:
                //selection  = "=?"
                selection = MemberEntry._ID + "=?";
                //заносим в массив значение id из URI
                //selectionArgs = 34 - подставляется вместо знака вопроса
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = db.delete(MemberEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                Toast.makeText(getContext(), "IncorrectURI", Toast.LENGTH_LONG).show();
                throw  new IllegalArgumentException("Can't update this URI " + uri);
        }
        if (rowsDeleted != 0)
            //уведомляет того кто вызвал этот метод через ContentResolver о вставке элемента по uri
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        String firstName = contentValues.getAsString(MemberEntry.COLUMN_FIRST_NAME);
        if (contentValues.containsKey(MemberEntry.COLUMN_FIRST_NAME))
            if (firstName == null)
                throw new IllegalArgumentException("Uncorrect input first name");
        String lastName = contentValues.getAsString(MemberEntry.COLUMN_LAST_NAME);
        if (contentValues.containsKey(MemberEntry.COLUMN_LAST_NAME))
            if (lastName == null)
                throw new IllegalArgumentException("Uncorrect input last name");
        Integer gender = contentValues.getAsInteger(MemberEntry.COLUMN_GENDER);
        if (contentValues.containsKey(MemberEntry.COLUMN_GENDER))
            if (gender == null || !(gender == MemberEntry.GENDER_UNKNOWN || gender == MemberEntry.GENDER_MALE || gender == MemberEntry.GENDER_FEMALE))
                throw new IllegalArgumentException("Uncorrect input gender");
        String sport = contentValues.getAsString(MemberEntry.COLUMN_SPORT);
        if (contentValues.containsKey(MemberEntry.COLUMN_SPORT))
            if (sport == null)
                throw new IllegalArgumentException("Uncorrect intput sport");
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int uriMatch = uriMatcher.match(uri);

        int rowsUpdated;
        switch (uriMatch){
            case MEMBERS:
                rowsUpdated =  db.update(MemberEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                break;
            case MEMBER_ID:
                //selection  = "=?"
                selection = MemberEntry._ID + "=?";
                //заносим в массив значение id из URI
                //selectionArgs = 34 - подставляется вместо знака вопроса
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsUpdated = db.update(MemberEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                break;
            default:
                Toast.makeText(getContext(), "IncorrectURI", Toast.LENGTH_LONG).show();
                throw  new IllegalArgumentException("Can't update this URI " + uri);
        }
        //если изменения в БД были, тогда вызываем метод уведомления
        if (rowsUpdated != 0)
            //уведомляет того кто вызвал этот метод через ContentResolver о вставке элемента по uri
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    @Override
    public String getType(Uri uri) {
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case MEMBERS:
                return MemberEntry.CONTENT_MULTIPLE_ITEMS;
            case MEMBER_ID:
               return MemberEntry.CONTENT_SINGLE_ITEM;
            default:
                throw  new IllegalArgumentException("Unknown URI " + uri);
        }
    }
}
