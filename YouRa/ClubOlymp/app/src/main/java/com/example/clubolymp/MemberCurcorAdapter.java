package com.example.clubolymp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.clubolymp.data.ClubOlimpContract.MemberEntry;

import java.util.zip.Inflater;


//CursorAdapter - предоставляет данные из Cursor(данные из БД SQLite) в виджет ListView
public class MemberCurcorAdapter extends CursorAdapter {

    public MemberCurcorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    //возвращает экземпляр View (в данном случае View из трех TextView) из xml-файла в Layout'е
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.member_item, parent, false);
    }

    //связываем элементы, которые расположены в View(три TextView) с данными из cursor(SQLite)
    //устанавливаем значения из данных элементов
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView firstNameTextView = view.findViewById(R.id.firstNameTextView);
        TextView lastNameTextView = view.findViewById(R.id.lastNameTextView);
        TextView sportTextView = view.findViewById(R.id.sportTextView);
        firstNameTextView.setText(cursor.getString(cursor.getColumnIndexOrThrow(MemberEntry.COLUMN_FIRST_NAME)));
        lastNameTextView.setText(cursor.getString(cursor.getColumnIndexOrThrow(MemberEntry.COLUMN_LAST_NAME)));
        sportTextView.setText(cursor.getString(cursor.getColumnIndexOrThrow(MemberEntry.COLUMN_SPORT)));
    }
}
