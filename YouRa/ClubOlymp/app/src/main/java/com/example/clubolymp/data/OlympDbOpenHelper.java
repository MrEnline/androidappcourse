package com.example.clubolymp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;
import com.example.clubolymp.data.ClubOlimpContract.MemberEntry;

public class OlympDbOpenHelper extends SQLiteOpenHelper {

    public OlympDbOpenHelper(@Nullable Context context) {
        super(context, ClubOlimpContract.DATABASE_NAME, null, ClubOlimpContract.VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + MemberEntry.TABLE_NAME + " ("
                + MemberEntry._ID + " INTEGER PRIMARY KEY,"
                + MemberEntry.COLUMN_FIRST_NAME + " TEXT,"
                + MemberEntry.COLUMN_LAST_NAME + " TEXT,"
                + MemberEntry.COLUMN_GENDER + " INTEGER NOT NULL,"
                + MemberEntry.COLUMN_SPORT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + ClubOlimpContract.MemberEntry.TABLE_NAME);
        onCreate(db);
    }
}
