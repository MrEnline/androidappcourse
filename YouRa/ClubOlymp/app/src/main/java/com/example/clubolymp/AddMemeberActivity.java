package com.example.clubolymp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.clubolymp.data.ClubOlimpContract.MemberEntry;
import com.example.clubolymp.data.OlympContentProvider;

import java.net.URI;
import java.util.ArrayList;

public class AddMemeberActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText sportEditText;
    private Spinner genderSpinner;
    private int gender = 0;
    private ArrayAdapter spinnerAdapter;
    private ArrayList<String> spinnerArrayList;
    private static final int EDIT_MEMBER_LOADER = 111;
    private Uri currentMemberUri;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_memeber);
        firstNameEditText = findViewById(R.id.fisrtNameEditText);
        lastNameEditText = findViewById(R.id.lastNameEditText);
        sportEditText = findViewById(R.id.groupEditText);
        genderSpinner = findViewById(R.id.genderSpinner);
        //получаем интент, который вызвал данную активити
        Intent intent = getIntent();
        currentMemberUri = intent.getData();
        if (currentMemberUri == null){
            setTitle("Add a member");   //устанавливаем заголовок экшнбара
            //если добавляем пользователя, то должна быть спрятан кнопка в меню "Delete member"
            invalidateOptionsMenu();    //для работы данного метода надо переопределить другой метод - onPrepareOptionsMenu
        }
        else{
            setTitle("Edit the Member");
            //загружаем и инициализируем loader только если хотим изменить контакт
            //getLoaderManager().initLoader(EDIT_MEMBER_LOADER, null, (android.app.LoaderManager.LoaderCallbacks<Cursor>)this);
            //deprecated method
            getSupportLoaderManager().initLoader(EDIT_MEMBER_LOADER, null, this);
        }

        //1-й метод создания списка в спиннере через ArrayList
/*        spinnerArrayList = new ArrayList();
        spinnerArrayList.add("Unknown");
        spinnerArrayList.add("Male");
        spinnerArrayList.add("Female");
        spinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(spinnerAdapter);*/

        //2-й метод создания списка в спиннере через ресурсы в R.array
        spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_gender, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(spinnerAdapter);

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedGender = (String)adapterView.getItemAtPosition(i);
                if (!TextUtils.isEmpty(selectedGender)){
                    if (selectedGender.equals("Male"))
                        gender = MemberEntry.GENDER_MALE;
                    else if (selectedGender.equals("Female"))
                        gender = MemberEntry.GENDER_FEMALE;
                    else
                        gender = MemberEntry.GENDER_UNKNOWN;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                gender = MemberEntry.GENDER_UNKNOWN;
            }
        });
    }

    //прячем настройку меню "Delete member"
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (currentMemberUri == null){
            MenuItem menuItem = menu.findItem(R.id.delete_member);
            menuItem.setVisible(false);
        }
        return true;
    }

    //метод создания меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_member_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_member:
                //insertMember();
                saveMember();
                return true;
            case R.id.delete_member:
                showDeleteMemberDialog();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        };
        return super.onOptionsItemSelected(item);
    }

    //public  void insertMember(){
    public void saveMember(){
        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();
        String sport = sportEditText.getText().toString().trim();
        //сделаем проверку ввода данных
        if (TextUtils.isEmpty(firstName)){
            Toast.makeText(this, "Input first name", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(lastName)){
            Toast.makeText(this, "Input last name", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(sport)){
            Toast.makeText(this, "Input sport", Toast.LENGTH_LONG).show();
            return;
        }
        if (gender == MemberEntry.GENDER_UNKNOWN){
            Toast.makeText(this, "Choose the gender", Toast.LENGTH_LONG).show();
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(MemberEntry.COLUMN_FIRST_NAME, firstName);
        contentValues.put(MemberEntry.COLUMN_LAST_NAME, lastName);
        contentValues.put(MemberEntry.COLUMN_GENDER, gender);
        contentValues.put(MemberEntry.COLUMN_SPORT, sport);

        //если Uri пустой, т.е. мы хотим добавить пользователя
        if (currentMemberUri == null){
            ContentResolver contentResolver = getContentResolver();
            Uri uri = contentResolver.insert(MemberEntry.CONTENT_URI, contentValues);
            if (uri == null)
                Toast.makeText(this, "Insertion of data in the table", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, "Data saved", Toast.LENGTH_LONG).show();
        }else{
            //иначе, если мы хотим изменить данные о пользователе
            int rowChanged = getContentResolver().update(currentMemberUri, contentValues,null, null);
            if (rowChanged == 0)
                Toast.makeText(this, "Saving of data in the table failed", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, "Member update", Toast.LENGTH_LONG).show();
        }

    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String[] projection = {
                MemberEntry._ID,
                MemberEntry.COLUMN_FIRST_NAME,
                MemberEntry.COLUMN_LAST_NAME,
                MemberEntry.COLUMN_GENDER,
                MemberEntry.COLUMN_SPORT
        };

        CursorLoader cursorLoader = new CursorLoader(this, currentMemberUri, projection,
                null, null, null);
        return cursorLoader;
    }

    //вызывается сразу после onCreateLoader
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()){
            int firstNameColumnIndex = cursor.getColumnIndex(MemberEntry.COLUMN_FIRST_NAME);
            int lastNameColumnIndex = cursor.getColumnIndex(MemberEntry.COLUMN_LAST_NAME);
            int genderColumnIndex = cursor.getColumnIndex(MemberEntry.COLUMN_GENDER);
            int sportColumnIndex = cursor.getColumnIndex(MemberEntry.COLUMN_SPORT);

            String firstName = cursor.getString(firstNameColumnIndex);
            String lastName = cursor.getString(lastNameColumnIndex);
            int gender = cursor.getInt(genderColumnIndex);
            String sport = cursor.getString(sportColumnIndex);

            firstNameEditText.setText(firstName);
            lastNameEditText.setText(lastName);
            sportEditText.setText(sport);

            switch (gender){
                case MemberEntry.GENDER_MALE:
                    genderSpinner.setSelection(MemberEntry.GENDER_MALE);
                    break;
                case MemberEntry.GENDER_FEMALE:
                    genderSpinner.setSelection(MemberEntry.GENDER_FEMALE);
                    break;
                default:
                    genderSpinner.setSelection(MemberEntry.GENDER_UNKNOWN);
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    private void showDeleteMemberDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want delete the member");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteMember();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteMember(){
        //если мы хотим удалить существующую запись
        if (currentMemberUri != null){
            int rowDeleted = getContentResolver().delete(currentMemberUri, null, null);
            if (rowDeleted == 0)
                Toast.makeText(this, "Deleting data from the table failed", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, "Member is deleted", Toast.LENGTH_LONG).show();
        }
        finish();
    }
}