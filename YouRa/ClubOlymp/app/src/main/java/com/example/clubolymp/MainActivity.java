package com.example.clubolymp;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.example.clubolymp.data.ClubOlimpContract.MemberEntry;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.net.URI;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private FloatingActionButton floatingActionButton;
    private ListView dataListView;
    MemberCurcorAdapter curcorAdapter;
    private static final int MEMBER_LOADER = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataListView = findViewById(R.id.dataListView);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddMemeberActivity.class);
                startActivity(intent);
            }
        });

        curcorAdapter = new MemberCurcorAdapter(this, null, false);
        //связываем ListView c тремя TextView и БД через cursorAdapter
        dataListView.setAdapter(curcorAdapter);
        getSupportLoaderManager().initLoader(MEMBER_LOADER, null, this);
        //установим слушатель на каждый элемент ListView
        dataListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //adapterView - сам ListView
            //view - отдельный элемент ListView
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, AddMemeberActivity.class);
                Uri currentMemberUri = ContentUris.withAppendedId(MemberEntry.CONTENT_URI, id);
                intent.setData(currentMemberUri);
                startActivity(intent);
            }
        });
    }

//    @Override
/*    protected void onStart() {
        super.onStart();
        displayData();
    }*/

    //метод для чтения данных из БД
/*    public  void displayData(){
        //заголовки столбцов, которые мы хотим получить из БД
        String[] projection = {
                MemberEntry._ID,
                MemberEntry.COLUMN_FIRST_NAME,
                MemberEntry.COLUMN_LAST_NAME,
                MemberEntry.COLUMN_GENDER,
                MemberEntry.COLUMN_SPORT
        };
        //вычитываем все столбцы со значениями из БД
        Cursor cursor = getContentResolver().query(MemberEntry.CONTENT_URI, projection, null, null, null);
        MemberCurcorAdapter cursorAdapter = new MemberCurcorAdapter(this, cursor, false);
        dataListView.setAdapter(cursorAdapter);


        *//*dataTextView.setText("All members\n\n");
        //сформируем заголовки столбцов таблиц для отображения
        dataTextView.append(MemberEntry._ID + " " +
                            MemberEntry.COLUMN_FIRST_NAME + " " +
                            MemberEntry.COLUMN_LAST_NAME + " " +
                            MemberEntry.COLUMN_GENDER + " " +
                            MemberEntry.COLUMN_SPORT);

        int idColumnIndex = cursor.getColumnIndex(MemberEntry._ID);
        int firstNameIndex = cursor.getColumnIndex(MemberEntry.COLUMN_FIRST_NAME);
        int lastNameIndex = cursor.getColumnIndex(MemberEntry.COLUMN_LAST_NAME);
        int genderIndex = cursor.getColumnIndex(MemberEntry.COLUMN_GENDER);
        int sportIndex = cursor.getColumnIndex(MemberEntry.COLUMN_SPORT);
        while (cursor.moveToNext()){
            int currentId = cursor.getInt(idColumnIndex);
            String currentFirstName = cursor.getString(firstNameIndex);
            String currentLastName = cursor.getString(lastNameIndex);
            int currentGender = cursor.getInt(genderIndex);
            String currentSport = cursor.getString(sportIndex);
            dataTextView.append("\n"+
                                currentId + " " +
                                currentFirstName + " " +
                                currentLastName + " " +
                                currentGender + " " +
                                currentSport);
        }*//*
    }*/

    //Возвращает загрузчик курсора по id
    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String[] projection = {
                MemberEntry._ID,
                MemberEntry.COLUMN_FIRST_NAME,
                MemberEntry.COLUMN_LAST_NAME,
                MemberEntry.COLUMN_GENDER,
                MemberEntry.COLUMN_SPORT
        };

        CursorLoader cursorLoader = new CursorLoader(this, MemberEntry.CONTENT_URI, projection,
                                                    null, null, null);
        return cursorLoader;
    }

 //вызывается сразу после работы метода onCreateLoader
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        //Swap in a new Cursor, returning the old Cursor. Unlike changeCursor(android.database.Cursor),
        // the returned old Cursor is not closed.
        //возвращает новый курсор, если произошли изменения, иначе возвращается старый
        curcorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        curcorAdapter.swapCursor(null);
    }
}