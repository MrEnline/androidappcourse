package com.example.retrofitdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.retrofitdemo.adapters.CountryAdapter;
import com.example.retrofitdemo.model.CountryInfo;
import com.example.retrofitdemo.model.Result;
import com.example.retrofitdemo.service.CountryService;
import com.example.retrofitdemo.service.RetrofitInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    ArrayList<Result> resultArrayList;
    CountryAdapter countryAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getCountries();
    }

    private Object getCountries(){
        CountryService countryService = RetrofitInstance.getService();
        Call<CountryInfo> call = countryService.getResults();
        call.enqueue(new Callback<CountryInfo>() {
            @Override
            public void onResponse(Call<CountryInfo> call, Response<CountryInfo> response) {
                CountryInfo countryInfo = response.body();
                if (countryInfo != null && countryInfo.getRestResponse() != null){
                    resultArrayList = (ArrayList<Result>) countryInfo.getRestResponse().getResult();
//                    for(Result result: resultArrayList){
//                        Log.i("resultArrayList", result.getName());
//                    }
                    fillRecyclerView();
                }
            }

            @Override
            public void onFailure(Call<CountryInfo> call, Throwable t) {
                Log.i("Error", t.getMessage());
            }
        });
        return resultArrayList;
    }

    private void fillRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        CountryAdapter countryAdapter = new CountryAdapter(resultArrayList);
        recyclerView.setAdapter(countryAdapter);
    }
}