package com.example.cooltimer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.preference.CheckBoxPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceGroup;
import androidx.preference.PreferenceScreen;

// интерфейс OnSharedPreferenceChangeListener необходим для слежения за изменениями
// в файле настроек SharePreference. Когда в него вносятся изменения, то выполняется
// метод onSharedPreferenceChanged

// интерфейс OnPreferenceChangeListener необходим для того, чтобы отслеживать изменения в отдельно взятой настройке
// при изменении какой-либо настройки вызывается метод onPreferenceChange, который не дает внести изменения,
// согласно заложенным в него условиям
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener,
        Preference.OnPreferenceChangeListener{

    //выполняется данный метод первым при создании экземпляра SettingsFragment
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.timer_preferences);
        //получаем ссылку на timer_preferences.xml (preferenceScreen)
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        //получаем ссылку на настройки, которые находятся в файле timer_preferences.xml
        SharedPreferences sharedPreferences = preferenceScreen.getSharedPreferences();
        int count = preferenceScreen.getPreferenceCount();
        for (int i = 0; i < count; i++){
            Preference preference = preferenceScreen.getPreference(i);
            if (!(preference instanceof CheckBoxPreference)){
                String value = sharedPreferences.getString(preference.getKey(), "");
                setPreferenceLabel(preference, value);
            }
        }
        //получим настройку за которой будем следить
        Preference preference = findPreference("default_interval");
        // если было произведено изменение для данной настройки, то вызывается onPreferenceChange
        preference.setOnPreferenceChangeListener(this);
    }

    //метод необходимый для отображения подсказок снизу основного текста в настройках
    private void setPreferenceLabel(Preference preference, String value){
        if (preference instanceof ListPreference){
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(value);
            if (index >= 0){
                listPreference.setSummary(listPreference.getEntries()[index]);
            }
        }
        if (preference instanceof EditTextPreference)
            preference.setSummary(value);
    }

    //необходим для моментального отображения выбранного звука в настройках под напдисью Timer Melody
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        Preference preference = findPreference(s);
        if (!(preference instanceof CheckBoxPreference)){
            String value = sharedPreferences.getString(preference.getKey(), "");
            setPreferenceLabel(preference, value);
        }
    }

    //создадим метод(слушатель) для регистрации onSharedPreferenceChanged
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //регистрируем callback-метод, который вызывается при изменении определенной раннее настройки
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    //разрегистрация слушателя
    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Toast toast = Toast.makeText(getContext(), "Please enter an integer number", Toast.LENGTH_LONG);
        if (preference.getKey().equals("default_interval")){
            String defaultIntervalString = (String) newValue;
            try {
                int defaultInterval = Integer.parseInt(defaultIntervalString);
            } catch (NumberFormatException nef){
                toast.show();
                return false;
            }
        }
        return true;
    }
}
