package com.example.cooltimer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SeekBar seekBar;
    private TextView textView;
    private Button button;
    private boolean bStart;
    private int maxTime = 600;
    private String defaultStrTimer = "00:30";
    CountDownTimer timer;
    private boolean isTimerOn;
    private int defaultInterval;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar = findViewById(R.id.seekBar);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);
        seekBar.setMax(maxTime);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setIntervalFromSharedPreference(sharedPreferences);
        //seekBar.setActivated(true);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateTime(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    public void onClickStartStop(View view) {
        if(!isTimerOn){
            isTimerOn = true;
            button.setText("Stop");
            timer = new CountDownTimer(seekBar.getProgress() * 1000, 1000) {

                @Override
                public void onTick(long l) {
                    updateTime(l/1000);
                    seekBar.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String timerMelody = preferences.getString("timer_melody", "bell");
                    MediaPlayer player;
                    switch (timerMelody){
                        case "bell_sound":
                            player = MediaPlayer.create(getApplicationContext(), R.raw.bell_sound);
                            player.start();
                            break;
                        case "alarm_siren_sound":
                            player = MediaPlayer.create(getApplicationContext(), R.raw.alarm_siren_sound);
                            player.start();
                            break;
                        case "bip_sound":
                            player = MediaPlayer.create(getApplicationContext(), R.raw.bip_sound);
                            player.start();
                            break;
                    }
/*                    if (preferences.getBoolean("sound_enable", true)){
                        MediaPlayer player = MediaPlayer.create(getApplicationContext(), R.raw.bell_sound);
                        player.start();
                    }*/
                    resetTimer();
                }
            };
            timer.start();
        }else{
            resetTimer();
        }
    }

    private void updateTime(long l){
        int minutes = (int)l / 60;
        int seconds = (int)l % 60;
        String minutesStr = String.valueOf(minutes);
        String secondStr = String.valueOf(seconds);
        if (String.valueOf(minutes).length() == 1)
            minutesStr = "0" + minutes;
        if (String.valueOf(seconds).length() == 1)
            secondStr = "0" + seconds;
        textView.setText(minutesStr + ":" + secondStr);
    }

    private void resetTimer(){
        button.setText("Start");
        timer.cancel();
        isTimerOn = false;
        seekBar.setEnabled(true);
        setIntervalFromSharedPreference(PreferenceManager.getDefaultSharedPreferences(this));
    }

    //создание меню в экшн баре
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.timer_menu, menu);
        return true;
    }

    //действия, которые выполняются при выборе одного из элементов меню
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_settings){
            Intent openSettings = new Intent(this, SettingsActivity.class);
            startActivity(openSettings);
        }
        if (id == R.id.action_about){
            Intent openAbout = new Intent(this, AboutActivity.class);
            startActivity(openAbout);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setIntervalFromSharedPreference(SharedPreferences sharedPreference){
        defaultInterval = Integer.valueOf(sharedPreference.getString("default_interval", "30"));
        int defaultIntervalMilliS = defaultInterval * 1000;
        updateTime(defaultInterval);
        seekBar.setProgress(defaultInterval);
    }

    //необходим для обновления editText с таймером после изменений настроек
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals("default_interval")){
            setIntervalFromSharedPreference(sharedPreferences);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }
}
