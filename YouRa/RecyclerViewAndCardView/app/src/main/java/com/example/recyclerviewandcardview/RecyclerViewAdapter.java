package com.example.recyclerviewandcardview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    ArrayList<RecyclerViewItem> recyclerViewItems;

    public RecyclerViewAdapter(ArrayList<RecyclerViewItem> recyclerViewItems) {
        this.recyclerViewItems = recyclerViewItems;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //получаем ссылку на разметку RecyclerViewItem
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        //создаем экземпляр класса RecyclerViewHolder и передаем в него ссылку на разметку
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    //связываем разметку в экземпляре класса RecyclerViewHolder с данными из массива recyclerViewItems
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        holder.textView1.setText(recyclerViewItems.get(position).getText1());
        holder.textView2.setText(recyclerViewItems.get(position).getText2());
        holder.imageView.setImageResource(recyclerViewItems.get(position).getImageResource());
    }

    @Override
    public int getItemCount() {
        return recyclerViewItems.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView textView1;
        public TextView textView2;

        //itemView - ссылка на разметку
        //связываем наши поля с разметков в CardView
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView1 = itemView.findViewById(R.id.textView1);
            textView2 = itemView.findViewById(R.id.textView2);
        }
    }
}
