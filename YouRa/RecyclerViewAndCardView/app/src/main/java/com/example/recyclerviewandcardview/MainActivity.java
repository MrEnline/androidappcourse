package com.example.recyclerviewandcardview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<RecyclerViewItem>  recyclerViewItems;
    private RecyclerView recyclerView;
    private  RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerViewItems = new ArrayList<>();
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Happy", "Life is fun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_bad_24, "Sad", "Life is sun!"));
        recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_baseline_mood_24, "Neutral", "Life is life!"));

        recyclerView = findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true); //улучшает производительность приложения, если фиксированный размер элементов
        adapter = new RecyclerViewAdapter(recyclerViewItems);
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }
}