package com.example.timers;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    static int seconds = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
/*        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 2000);
                Log.d("tag", "Delay 2000 ms");
            }
        };
        handler.post(runnable);*/

        CountDownTimer myTimer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long l) {
                textView.setText(String.format("%s", l/1000));
            }

            @Override
            public void onFinish() {
                textView.setText("Операция закончилась");
            }
        };
        myTimer.start();
    }
}