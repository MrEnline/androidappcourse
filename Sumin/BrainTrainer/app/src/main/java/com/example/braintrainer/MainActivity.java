package com.example.braintrainer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    
    TextView textViewTimer;
    TextView textViewQuestion;
    TextView textViewScore;
    TextView textViewOpinion0;
    TextView textViewOpinion1;
    TextView textViewOpinion2;
    TextView textViewOpinion3;
    ArrayList<TextView> option;
    int min = 5;
    int max = 30;
    boolean isPositive;
    int rightAnswer;
    int rightAnswerPosition;
    int countOfQuestion = 0;
    int countOfRightAnswer = 0;
    String question;
    boolean gameOver = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewTimer = findViewById(R.id.textViewTimer);
        textViewQuestion = findViewById(R.id.textViewQuestion);
        textViewScore = findViewById(R.id.textViewScore);
        textViewOpinion0 = findViewById(R.id.textViewOpinion0);
        textViewOpinion1 = findViewById(R.id.textViewOpinion1);
        textViewOpinion2 = findViewById(R.id.textViewOpinion2);
        textViewOpinion3 = findViewById(R.id.textViewOpinion3);
        option = new ArrayList<>();
        option.add(textViewOpinion0);
        option.add(textViewOpinion1);
        option.add(textViewOpinion2);
        option.add(textViewOpinion3);
        playNext();
        CountDownTimer timer = new CountDownTimer(15000, 1000) {
            @Override
            public void onTick(long l) {
                textViewTimer.setText(getTime(l));
                if (l < 10000)
                    textViewTimer.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }

            @Override
            public void onFinish() {
                textViewTimer.setText(String.format(Locale.getDefault(),"%02d:%02d", 0, 0));
                gameOver = true;
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                int max = preferences.getInt("max", 0);
                if (countOfRightAnswer > max)
                    preferences.edit().putInt("max", countOfRightAnswer).apply();
                Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
                intent.putExtra("result", countOfRightAnswer);
                startActivity(intent);
            }
        };
        timer.start();
    }

    private  void  playNext(){
        generateQuestion();
        for (int i = 0; i < option.size(); i++){
            if (i == rightAnswerPosition){
                option.get(i).setText(Integer.toString(rightAnswer));
            }else{
                option.get(i).setText(Integer.toString(generateWrongAnswer()));
            }
        }
        String score = String.format("%s / %s", countOfRightAnswer, countOfQuestion);
        textViewScore.setText(score);
    }

    private void generateQuestion(){
        int a = (int) (Math.random() * (max - min + 1) + min);
        int b = (int) (Math.random() * (max - min + 1) + min);
        int mark = (int) (Math.random() * 2);
        isPositive = mark == 1;
        if (isPositive){
            rightAnswer = a + b;
            question = String.format("%s + %s", a, b);
        }else{
            rightAnswer = a - b;
            question = String.format("%s - %s", a, b);
        }
        textViewQuestion.setText(question);
        rightAnswerPosition = (int) (Math.random() * 4);
    }

    private int generateWrongAnswer(){
        int result;
        do{
            result = (int) (Math.random() * max * 2 - 1) - (max - min);
        }while (result == rightAnswer);
        return result;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private  String getTime(long millis){
        int seconds = (int) millis/1000;
        int minutes = seconds / 60;
        seconds = seconds % 60;
        return String.format(Locale.getDefault(),"%02d:%02d", minutes, seconds);
    }

    public void onClickAnswer(View view) {
        TextView answer = (TextView)view;
        int result = Integer.parseInt(answer.getText().toString());
        if (!gameOver){
            if (result == rightAnswer) {
                countOfRightAnswer++;
                Toast.makeText(this, "Верно", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(this, "Не верно", Toast.LENGTH_SHORT).show();
            }
            countOfQuestion++;
            playNext();
        }

    }
}