package com.example.testemploees.screens.emploees;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.testemploees.R;
import com.example.testemploees.adapters.EmploeeAdapter;
import com.example.testemploees.api.ApiFactory;
import com.example.testemploees.api.ApiService;
import com.example.testemploees.pojo.Emploee;
import com.example.testemploees.pojo.EmploeeResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class EmploeeListActivity extends AppCompatActivity implements EmploeeListView {

    private RecyclerView recyclerView;
    private EmploeeAdapter emploeeAdapter;
    private List<Emploee> emploees;
    private EmploeeListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerViewEmploee);
        emploees = new ArrayList<>();
        presenter = new EmploeeListPresenter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        emploeeAdapter = new EmploeeAdapter();
        emploeeAdapter.setEmploees(emploees);
        recyclerView.setAdapter(emploeeAdapter);
        presenter.loadData();
    }

    public void showData(List<Emploee> emploees){
        emploeeAdapter.setEmploees(emploees);
    }

    public void showError(){
        Toast.makeText(this, "Error load data", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.disposeDisposable();
    }
}