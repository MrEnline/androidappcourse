package com.example.messenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ReceiveMessageActivity extends AppCompatActivity {

    private TextView textViewReceiveMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_message);
        textViewReceiveMsg = findViewById(R.id.textViewReceiveMsg);
        Intent intent = getIntent();
        String msg = intent.getStringExtra("msg");
        textViewReceiveMsg.setText(msg);
    }
}