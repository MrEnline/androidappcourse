package com.example.messenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editTextMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextMsg = findViewById(R.id.editMessage);
    }

    public void onClickSendMsg(View view) {
        //Intent intent = new Intent(this, ReceiveMessageActivity.class);
        String msg = editTextMsg.getText().toString();
        //intent.putExtra("msg", msg);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        Intent choseIntent = Intent.createChooser(intent, getString(R.string.chosen_title));
        startActivity(choseIntent);
    }
}