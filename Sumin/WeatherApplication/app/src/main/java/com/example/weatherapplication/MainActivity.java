package com.example.weatherapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DownloadJSONTask task = new DownloadJSONTask();
        task.execute("http://api.openweathermap.org/data/2.5/weather?q=London&appid=8c5eb87f05b913351355a302ab0b5a97");
    }

    private static class DownloadJSONTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(strings[0]);
                StringBuilder result = new StringBuilder();
                urlConnection = (HttpURLConnection)url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader buffer = new BufferedReader(inputStreamReader);
                String line = buffer.readLine();
                while (line != null){
                    result.append(line);
                    line = buffer.readLine();
                }
                return  result.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                //Log.i("Result", jsonObject.getString("name"));
                //Log.i("Result", jsonObject.getJSONObject("main").getString("pressure"));
                String tmp = jsonObject.getJSONArray("weather").getJSONObject(0).getString("description");
                Log.i("Result", tmp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}