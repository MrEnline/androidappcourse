package com.example.timestable;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listViewTimes;
    private SeekBar seekBarTimes;
    private Integer max = 20;
    private Integer min = 1;
    private Integer count = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ArrayList<Integer> numbers = new ArrayList<>();
        listViewTimes = findViewById(R.id.listViewTimes);
        seekBarTimes = findViewById(R.id.seekBarTimes);
        seekBarTimes.setMax(max);
        final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, numbers);
        listViewTimes.setAdapter(arrayAdapter);
        seekBarTimes.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i < min)
                    seekBarTimes.setProgress(min);
                numbers.clear();
                for (int j = min; j < count; j++) {
                    numbers.add(seekBar.getProgress() * j);
                }
                arrayAdapter.notifyDataSetChanged();
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarTimes.setProgress(10);
    }
}