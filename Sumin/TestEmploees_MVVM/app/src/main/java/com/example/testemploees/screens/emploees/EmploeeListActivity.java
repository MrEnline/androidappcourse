package com.example.testemploees.screens.emploees;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.testemploees.R;
import com.example.testemploees.adapters.EmploeeAdapter;
import com.example.testemploees.pojo.Emploee;
import com.example.testemploees.pojo.Speciality;

import java.util.ArrayList;
import java.util.List;

public class EmploeeListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    EmploeeAdapter emploeeAdapter;
    List<Emploee> emploees;
    EmploeeViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerViewEmploee);
        emploees = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        emploeeAdapter = new EmploeeAdapter();
        emploeeAdapter.setEmploees(emploees);
        recyclerView.setAdapter(emploeeAdapter);
        viewModel = new ViewModelProvider(new ViewModelStore(), new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(EmploeeViewModel.class);
        //подписываемся на обновления LiveData в классе EmploeeViewModel
        viewModel.getEmployees().observe(this, new Observer<List<Emploee>>() {
            @Override
            public void onChanged(List<Emploee> emploees) {
                emploeeAdapter.setEmploees(emploees);
                if (emploees != null && emploees.size() > 0){
                    for (Emploee emploee: emploees){
                        List<Speciality> specialities = emploee.getSpecialty();
                        for (Speciality speciality: specialities){
                            Log.i("Speciality", speciality.getSpecialtyId() + " - " + speciality.getName());
                        }
                    }
                }
            }
        });
        viewModel.getErrors().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(@Nullable Throwable throwable) {
                if (throwable != null) {
                    Toast.makeText(EmploeeListActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    viewModel.clearErrors();
                }
            }
        });
        viewModel.loadData();
    }
}