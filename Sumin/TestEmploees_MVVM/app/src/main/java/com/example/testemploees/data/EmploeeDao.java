package com.example.testemploees.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.testemploees.pojo.Emploee;

import java.util.List;

@Dao
public interface EmploeeDao {
    @Query("SELECT * FROM emploees2")
    LiveData<List<Emploee>> getAllEmploees();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEmploees(List<Emploee> emploees);

    @Query("DELETE FROM emploees2")
    void deleteAllEmploees();
}
