package com.example.testemploees.converters;

import androidx.room.TypeConverter;

import com.example.testemploees.pojo.Speciality;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    //конвертируем лист со специальностями в строку
    @TypeConverter
    public String listSpecialityToString(List<Speciality> specialities){
//        JSONArray jsonArray = new JSONArray();
//        for (Speciality speciality:specialities) {
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("specialty_id", speciality.getSpecialtyId());
//                jsonObject.put("name", speciality.getName());
//            }catch (JSONException e){
//                e.printStackTrace();
//            }
//        }
//        return jsonArray.toString();
        //аналог вышеописанному коду
        return new Gson().toJson(specialities);
    }

    //конвертируем строку в лист со специальностями
    @TypeConverter
    public List<Speciality> stringToListSpeciality(String specialityAsString){
        Gson gson = new Gson();
        ArrayList objects = gson.fromJson(specialityAsString, ArrayList.class);
        ArrayList<Speciality> specialities = new ArrayList<>();
        for (Object o: objects) {
            specialities.add(gson.fromJson(o.toString(), Speciality.class));
        }
        return specialities;
    }
}
