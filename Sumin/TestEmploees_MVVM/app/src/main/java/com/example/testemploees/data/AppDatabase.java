package com.example.testemploees.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.testemploees.pojo.Emploee;

@Database(entities = {Emploee.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase database;
    private static final String DB_NAME = "emploees2.db";
    private static Object lock = new Object();

    public static AppDatabase getInstance(Context context){
        synchronized (lock){
            if (database == null){
                database = Room.databaseBuilder(context, AppDatabase.class, DB_NAME).fallbackToDestructiveMigration().build();
                return database;
            }
        }
        return database;
    }

    public abstract EmploeeDao emploeeDao();
}
