package com.example.testemploees.screens.emploees;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.testemploees.api.ApiFactory;
import com.example.testemploees.api.ApiService;
import com.example.testemploees.data.AppDatabase;
import com.example.testemploees.pojo.Emploee;
import com.example.testemploees.pojo.EmploeeResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class EmploeeViewModel extends AndroidViewModel {

    LiveData<List<Emploee>> employees;
    private static AppDatabase database;
    private MutableLiveData<Throwable> errors;

    Disposable disposable;
    CompositeDisposable compositeDisposable;

    public EmploeeViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(application);
        employees = database.emploeeDao().getAllEmploees();
        errors = new MutableLiveData<>();
    }

    public LiveData<List<Emploee>> getEmployees() {
        return employees;
    }

    public LiveData<Throwable> getErrors() {
        return errors;
    }

    public void clearErrors(){
        errors.setValue(null);
    }

    private void insertAllEmloees(List<Emploee> emploees){
        new AsyncInsertAllEmploees().execute(emploees);
    }

    private void deleteAllEmploees(){
        new AsyncDeleteAllEmploees().execute();
    }

    private static class AsyncInsertAllEmploees extends AsyncTask<List<Emploee>, Void, Void>{
        @Override
        protected Void doInBackground(List<Emploee>... lists) {
            if (lists != null && lists.length > 0){
                database.emploeeDao().insertEmploees(lists[0]);
            }
            return null;
        }
    }

    private static class AsyncDeleteAllEmploees extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            database.emploeeDao().deleteAllEmploees();
            return null;
        }
    }

    public void loadData(){
        ApiFactory apiFactory = ApiFactory.getApiFactory();
        ApiService apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();
        disposable = apiService.getEmploees().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<EmploeeResponse>() {
                    @Override
                    public void accept(EmploeeResponse emploeeResponse) throws Exception {
                        deleteAllEmploees();
                        insertAllEmloees(emploeeResponse.getResponse());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        errors.setValue(throwable);
                    }
                });
        compositeDisposable.add(disposable);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
