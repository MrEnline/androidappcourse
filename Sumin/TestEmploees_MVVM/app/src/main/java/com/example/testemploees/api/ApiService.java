package com.example.testemploees.api;

import com.example.testemploees.pojo.EmploeeResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {
    @GET("testTask.json")
    Observable<EmploeeResponse> getEmploees();
    //для приложения MyMovies - указываем параметры при запросе
    //Observable<EmploeeResponse> getEmploees(@Query("api_key") int apiKey, @Query("lang") String lang);

    //для приложения MyMovies - указываем параметры при запросе
    //@GET("testTask.json/{id}/video")
    //Observable<EmploeeResponse> getEmploees(@Path("id") int id, @Query("api_key") int apiKey, @Query("lang") String lang);

}
