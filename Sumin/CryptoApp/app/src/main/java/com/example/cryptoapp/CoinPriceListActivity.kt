package com.example.cryptoapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.cryptoapp.adapters.CoinInfoAdapter
import com.example.cryptoapp.pojo.CoinPriceInfo
import kotlinx.android.synthetic.main.activity_coin_price_list.*

class CoinPriceListActivity : AppCompatActivity() {
    private lateinit var viewModel: CoinViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coin_price_list)
        //viewModel = ViewModelProviders.of(this).get(CoinViewModel::class.java)
        val adapter = CoinInfoAdapter(this)
        adapter.onCoinClickListener = object: CoinInfoAdapter.OnCoinClickListener{
            override fun onCoinClick(coinPriceInfo: CoinPriceInfo) {
                //val intent = Intent(this@CoinPriceListActivity, CoinDetailActivity::class.java)
                //intent.putExtra(CoinDetailActivity.EXTRA_FROM_SYMBOL, coinPriceInfo.fromSymbol)
                //Log.d("FromSymbol", coinPriceInfo.fromSymbol)
                val intent = CoinDetailActivity.newIntent(this@CoinPriceListActivity, coinPriceInfo.fromSymbol)
                startActivity(intent)
            }
        }
        rvCoinPriceList.adapter = adapter
        //viewModel = ViewModelProviders.of(this)[CoinViewModel::class.java]
        //viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(CoinViewModel::class.java)
        viewModel = ViewModelProvider(this)[CoinViewModel::class.java]
        //viewModel.loadData()
        //viewModel.priceList.observe(this, Observer { Log.d("TEST_OF_LOADING_DATA", "Success in Activity: $it") })
        //viewModel.getDetailInfo(fSym = "BTC").observe(this, Observer { Log.d("TEST_OF_LOADING_DATA", "Success in Activity: $it") })
        viewModel.priceList.observe(this, Observer {
            adapter.сoinInfoList = it
        })
    }
}
