package com.example.cryptoapp.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.gson.JsonObject

data class CoinPriceInfoRawData (@SerializedName("RAW")
                                 @Expose
                                 val coinPriceInfoRawData: JsonObject? = null)