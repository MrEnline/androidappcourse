package com.example.cryptoapp.pojo

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.cryptoapp.api.ApiFactory.BASE_IMAGE_URL
import com.example.cryptoapp.utils.convertTimestampToTime
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
@Entity(tableName = "full_price_list")
data class CoinPriceInfo (
    @SerializedName("TYPE")
    @Expose
    val type: String?,
    @SerializedName("MARKET")
    @Expose
    val market: String?,
    @SerializedName("FROMSYMBOL")
    @Expose
    @PrimaryKey
    val fromSymbol: String,
    @SerializedName("TOSYMBOL")
    @Expose
    val toSymbol: String?,
    @SerializedName("FLAGS")
    @Expose
    val flags: String?,
    @SerializedName("PRICE")
    @Expose
    val price: String?,
    @SerializedName("LASTUPDATE")
    @Expose
    val lastUpdate: Long?,
    @SerializedName("LASTVOLUME")
    @Expose
    val lastVolume: String?,
    @SerializedName("LASTVOLUMETO")
    @Expose
    val lastVolumeTo: String?,
    @SerializedName("LASTTRADEID")
    @Expose
    val lastTradeId: String?,
    @SerializedName("VOLUMEDAY")
    @Expose
    val volumeDay: String?,
    @SerializedName("VOLUMEDAYTO")
    @Expose
    val volumeDayTo: String?,
    @SerializedName("VOLUME24HOUR")
    @Expose
    val volume24Hour: String?,
    @SerializedName("VOLUME24HOURTO")
    @Expose
    val volume24HourTo: String?,
    @SerializedName("OPENDAY")
    @Expose
    val openDay: String?,
    @SerializedName("HIGHDAY")
    @Expose
    val highDay: String?,
    @SerializedName("LOWDAY")
    @Expose
    val lowDay: String?,
    @SerializedName("OPEN24HOUR")
    @Expose
    val open24Hour: String?,
    @SerializedName("HIGH24HOUR")
    @Expose
    val high24Hour: String?,
    @SerializedName("LOW24HOUR")
    @Expose
    val low24Hour: String?,
    @SerializedName("LASTMARKET")
    @Expose
    val lastMarket: String?,
    @SerializedName("VOLUMEHOUR")
    @Expose
    val volumeHour: String?,
    @SerializedName("VOLUMEHOURTO")
    @Expose
    val volumeHourTo: String?,
    @SerializedName("OPENHOUR")
    @Expose
    val openHour: String?,
    @SerializedName("HIGHHOUR")
    @Expose
    val highHour: String?,
    @SerializedName("LOWHOUR")
    @Expose
    val lowHour: String?,
    @SerializedName("TOPTIERVOLUME24HOUR")
    @Expose
    val topTierVolume24Hour: String?,
    @SerializedName("TOPTIERVOLUME24HOURTO")
    @Expose
    val topTierVolume24HourTo: String?,
    @SerializedName("CHANGE24HOUR")
    @Expose
    val change24Hour: String?,
    @SerializedName("CHANGEPCT24HOUR")
    @Expose
    val changePCT24Hour: String?,
    @SerializedName("CHANGEDAY")
    @Expose
    val changeDay: String?,
    @SerializedName("CHANGEPCTDAY")
    @Expose
    val changePCTDay: String?,
    @SerializedName("SUPPLY")
    @Expose
    val supply: String?,
    @SerializedName("MKTCAP")
    @Expose
    val mktCap: String?,
    @SerializedName("TOTALVOLUME24H")
    @Expose
    val totalVolume24Hour: String?,
    @SerializedName("TOTALVOLUME24HTO")
    @Expose
    val totalVolume24HourTo: String?,
    @SerializedName("TOTALTOPTIERVOLUME24H")
    @Expose
    val totalTopTierVolume24Hour: String?,
    @SerializedName("TOTALTOPTIERVOLUME24HTO")
    @Expose
    val totalTopTierVolume24HourTo: String?,
    @SerializedName("IMAGEURL")
    @Expose
    val imageUrl: String?
){
    fun getFormattedTime():String{
        return convertTimestampToTime(lastUpdate)
    }

    fun getFullImageUrl(): String{
        return BASE_IMAGE_URL + imageUrl
    }
}
//(
//    @SerializedName("TYPE")
//    @Expose
//    val type: String? = null,
//
//    @SerializedName("MARKET")
//    @Expose
//    val market: String? = null,
//
//    @SerializedName("FROMSYMBOL")
//    @Expose
//    val fromSymbol: String? = null,
//
//    @SerializedName("TOSYMBOL")
//    @Expose
//    val toSymbol: String? = null,
//
//    @SerializedName("FLAGS")
//    @Expose
//    val flags: String? = null,
//
//    @SerializedName("PRICE")
//    @Expose
//    val price: Double? = null,
//
//    @SerializedName("LASTUPDATE")
//    @Expose
//    val lastUpdate: Int? = null,
//
//    @SerializedName("MEDIAN")
//    @Expose
//    val median: Double? = null,
//
//    @SerializedName("LASTVOLUME")
//    @Expose
//    val lastVolume: Double? = null,
//
//    @SerializedName("LASTVOLUMETO")
//    @Expose
//    val lastVolumeTo: Double? = null,
//
//    @SerializedName("LASTTRADEID")
//    @Expose
//    val lastTradeId: String? = null,
//
//    @SerializedName("VOLUMEDAY")
//    @Expose
//    val volumeDay: Double? = null,
//
//    @SerializedName("VOLUMEDAYTO")
//    @Expose
//    val volumeDayTo: Double? = null,
//
//    @SerializedName("VOLUME24HOUR")
//    @Expose
//    val volume24Hour: Double? = null,
//
//    @SerializedName("VOLUME24HOURTO")
//    @Expose
//    val volume24HourTo: Double? = null,
//
//    @SerializedName("OPENDAY")
//    @Expose
//    val openDay: Double? = null,
//
//    @SerializedName("HIGHDAY")
//    @Expose
//    val highDay: Double? = null,
//
//    @SerializedName("LOWDAY")
//    @Expose
//    val lowDay: Double? = null,
//
//    @SerializedName("OPEN24HOUR")
//    @Expose
//    val open24Hour: Double? = null,
//
//    @SerializedName("HIGH24HOUR")
//    @Expose
//    val high24Hour: Double? = null,
//
//    @SerializedName("LOW24HOUR")
//    @Expose
//    val low24Hour: Double? = null,
//
//    @SerializedName("LASTMARKET")
//    @Expose
//    val lastMarket: String? = null,
//
//    @SerializedName("VOLUMEHOUR")
//    @Expose
//    val volumeHour: Double? = null,
//
//    @SerializedName("VOLUMEHOURTO")
//    @Expose
//    val volumeHourTo: Double? = null,
//
//    @SerializedName("OPENHOUR")
//    @Expose
//    val openHour: Double? = null,
//
//    @SerializedName("HIGHHOUR")
//    @Expose
//    val highHour: Double? = null,
//
//    @SerializedName("LOWHOUR")
//    @Expose
//    val lowHour: Double? = null,
//
//    @SerializedName("TOPTIERVOLUME24HOUR")
//    @Expose
//    val topTierVolume24Hour: Double? = null,
//
//    @SerializedName("TOPTIERVOLUME24HOURTO")
//    @Expose
//    val topTierVolume24HourTo: Double? = null,
//
//    @SerializedName("CHANGE24HOUR")
//    @Expose
//    val change24Hour: Double? = null,
//
//    @SerializedName("CHANGEPCT24HOUR")
//    @Expose
//    val changePct24Hour: Double? = null,
//
//    @SerializedName("CHANGEDAY")
//    @Expose
//    val changeDay: Double? = null,
//
//    @SerializedName("CHANGEPCTDAY")
//    @Expose
//    val changePctDay: Double? = null,
//
//    @SerializedName("CHANGEHOUR")
//    @Expose
//    val changeHour: Double? = null,
//
//    @SerializedName("CHANGEPCTHOUR")
//    @Expose
//    val changePctHour: Double? = null,
//
//    @SerializedName("CONVERSIONTYPE")
//    @Expose
//    val conversionType: String? = null,
//
//    @SerializedName("CONVERSIONSYMBOL")
//    @Expose
//    val conversionSymbol: String? = null,
//
//    @SerializedName("SUPPLY")
//    @Expose
//    val supply: Int? = null,
//
//    @SerializedName("MKTCAP")
//    @Expose
//    val mktCap: Double? = null,
//
//    @SerializedName("MKTCAPPENALTY")
//    @Expose
//    val mktCapPenalty: Int? = null,
//
//    @SerializedName("CIRCULATINGSUPPLY")
//    @Expose
//    val circulatingSupply: Int? = null,
//
//    @SerializedName("CIRCULATINGSUPPLYMKTCAP")
//    @Expose
//    val circulatingSupplyMktCap: Double? = null,
//
//    @SerializedName("TOTALVOLUME24H")
//    @Expose
//    val totalVolume24H: Double? = null,
//
//    @SerializedName("TOTALVOLUME24HTO")
//    @Expose
//    val totalVolume24HTo: Double? = null,
//
//    @SerializedName("TOTALTOPTIERVOLUME24H")
//    @Expose
//    val totalTopTierVolume24H: Double? = null,
//
//    @SerializedName("TOTALTOPTIERVOLUME24HTO")
//    @Expose
//    val totalTopTierVolume24HTo: Double? = null,
//
//    @SerializedName("IMAGEURL")
//    @Expose
//    val imageUrl: String? = null
//)