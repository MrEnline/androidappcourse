package com.example.guessstar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    ImageView imageViewStar;
    TextView textView;
    ArrayList<String> images;
    ArrayList<String> names;
    ArrayList<Button> buttons;
    ArrayList<Integer> cache;
    int numberOfQuestion;
    int numberOfRightAnswer;
    int count;
    private  String url = "https://www.ivi.ru/titr/motor/best-actors-of-the-21st-century";
    private  String mainURL = "https://ivi.ru/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        imageViewStar = findViewById(R.id.imageViewStar);
        textView = findViewById(R.id.textViewCount);
        images = new ArrayList<>();
        names = new ArrayList<>();
        buttons = new ArrayList<>();
        buttons.add(button0);
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        cache = new ArrayList<>();
        count = 0;
        getContent();
        playGame();
    }

    private void playGame(){
        generateQuestion();
        DownloadImageTask task = new DownloadImageTask();
        try {
            Bitmap bitmap = task.execute(images.get(numberOfQuestion)).get();
            if (bitmap != null){
                imageViewStar.setImageBitmap(bitmap);
                count++;
                textView.setText("Вопрос № " + count);
                for (int i = 0; i < buttons.size(); i++){
                    if (i == numberOfRightAnswer){
                        buttons.get(i).setText(names.get(numberOfQuestion));
                    }else{
                        buttons.get(i).setText(names.get(generateWrongAnswer()));
                    }
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private  void generateQuestion(){
        boolean flag = false;
        while (true){
        numberOfQuestion = (int) (Math.random() * names.size());
        if(!cache.contains(numberOfQuestion)){
            cache.add(numberOfQuestion);
            break;
        }
        if (cache.size() == names.size() - 5){
            for (int i = 0; i < names.size(); i++){
                if (!cache.contains(i)){
                    cache.add(numberOfQuestion);
                    flag = true;
                    break;
                }
            }
            if (flag){
                flag = false;
                break;
            }
        }
    }
    numberOfRightAnswer = (int) (Math.random() * buttons.size());
    }

    private  int generateWrongAnswer(){
        return  (int) (Math.random() * names.size());
    }

    private  void  getContent(){
        DownloadContentTask task = new  DownloadContentTask();
        try {
            String content = task.execute(url).get();
            //Log.i("MyResult", content);
            String start = "<div itemprop=\"description\" class=\"article-text\">";
            String finish = "div class=\"report-error\">";
            Pattern pattern = Pattern.compile(start + "(.*?)" + finish);
            Matcher matcher = pattern.matcher(content);
            String splitContent = "";
            while (matcher.find()){
                splitContent = matcher.group(1);
            }
            Pattern patternImg = Pattern.compile("<img src=\"" + "(.*?)" + "\">");
            Matcher matcherImg = patternImg.matcher(splitContent);
            while (matcherImg.find()){
                images.add(mainURL + matcherImg.group(1));
            }
            Pattern patternName = Pattern.compile("<a href=\"/" + "(.*?)" + "\">" + "(.*?)" + "</a>");
            Matcher matcherName = patternName.matcher(splitContent);
            while (matcherName.find()){
                names.add(matcherName.group(2));
            }
            for (String name: names) {
                Log.i("MyResult", name);
            }
            //Log.i("MyResult", splitContent);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onClickAnswer(View view) {
        Button button = (Button) view;
        int numTag = Integer.parseInt(button.getTag().toString());
        if (numTag == numberOfRightAnswer){
            Toast.makeText(this, "Верно", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Не верно", Toast.LENGTH_SHORT).show();
        }
        if (count == names.size()){
            Toast.makeText(this, "Игра окончена", Toast.LENGTH_LONG).show();
        }
        playGame();
    }

    private static class DownloadContentTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            StringBuilder result = new StringBuilder();
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String line = bufferedReader.readLine();
                while (line != null){
                    result.append(line);
                    line = bufferedReader.readLine();
                }
                return result.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return null;
        }
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap>{

        @Override
        protected Bitmap doInBackground(String... strings) {
            StringBuilder result = new StringBuilder();
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return null;
        }
    }
}