package com.example.appsimpletimer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private boolean isRunnig;
    private  boolean wasRunning;
    private int second;
    private TextView textViewTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewTimer = findViewById(R.id.textViewTimer);
        if (savedInstanceState != null){
            second = savedInstanceState.getInt("second");
            isRunnig = savedInstanceState.getBoolean("isRunning");
            wasRunning = savedInstanceState.getBoolean("wasRunning");
        }
        runTimer();
    }

    //строчка для файла AndroidManifest.xml
//android:configChanges="screenSize|orientation"

    //сохраняет состояние(значения переменных) Активити перед уничтожением при
    // изменении конфигурации(например при изменении размеров экрана или повороте дисплея)
    //в переменной типа Bundle в виде ключ-значение
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("second", second);
        outState.putBoolean("isRunning", isRunnig);
        outState.putBoolean("wasRunning", wasRunning);
    }

    public void onClickStartTimer(View view) {
        isRunnig = true;
    }

    public void onClickPauseTimer(View view) {
        isRunnig = false;
    }

    public void onClickResetTimer(View view) {
        isRunnig = false;
        second = 0;
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        isRunnig = wasRunning;
//    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        wasRunning = isRunnig;
//        isRunnig = false;
//    }

    @Override
    protected void onPause() {
        super.onPause();
        wasRunning = isRunnig;
        isRunnig = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunnig = wasRunning;
    }

    //объект Handler используется для планирования выполнения и
    //передачи кода другому потоку
    private  void  runTimer(){
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                int hours = second / 3600;
                int minutes = (second % 3600) / 60;
                int secs = second % 60;

                String time = String.format(Locale.getDefault(),"%d:%02d:%02d", hours, minutes,secs);
                textViewTimer.setText(time);

                if(isRunnig)
                    second++;

                handler.postDelayed(this, 1000);
            }
        });
    }
}