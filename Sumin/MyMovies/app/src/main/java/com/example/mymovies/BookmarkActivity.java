package com.example.mymovies;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymovies.adapters.MovieAdapter;
import com.example.mymovies.data.BookmarkMovie;
import com.example.mymovies.data.MainViewModel;
import com.example.mymovies.data.Movie;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class BookmarkActivity extends AppCompatActivity {
    private RecyclerView recyclerViewBookmarkMovies;
    private MovieAdapter movieAdapter;
    private MainViewModel viewModel;
    private FloatingActionButton actionButton;

    private int getColumnCount(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int witdh = (int) (displayMetrics.widthPixels / displayMetrics.density);
        return witdh / 185 > 2 ? witdh / 185 : 2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        recyclerViewBookmarkMovies = findViewById(R.id.recyclerViewBookmarkMovies);
        actionButton = findViewById(R.id.floatingActionButtonBookmarkFilm);
        recyclerViewBookmarkMovies.setLayoutManager(new GridLayoutManager(this,getColumnCount()));
        movieAdapter = new MovieAdapter(false);
        recyclerViewBookmarkMovies.setAdapter(movieAdapter);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(R.string.myFilmsBookmark);
        viewModel = new ViewModelProvider(new ViewModelStore(), new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(MainViewModel.class);
        movieAdapter.setOnPosterClickListener(new MovieAdapter.OnPosterClickListener() {
            @Override
            public void onPosterClick(int position) {
                Movie movie = movieAdapter.getMovies().get(position);
                Intent intent = new Intent(BookmarkActivity.this, DetailActivity.class);
                intent.putExtra("id", movie.getId());
                intent.putExtra("nameActivity", "BookmarkActivity");
                startActivity(intent);
            }
        });
        LiveData<List<BookmarkMovie>> bookmarkMovies = viewModel.getBookmarkMovies();
        bookmarkMovies.observe(this, new Observer<List<BookmarkMovie>>() {
            @Override
            public void onChanged(List<BookmarkMovie> bookmarkMovies) {
                List<Movie> movieList = new ArrayList<>();
                if (bookmarkMovies != null){
                    movieList.addAll(bookmarkMovies);
                    movieAdapter.setMovies(movieList);
                }
            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewBookmarkMovies.smoothScrollToPosition(0);
            }
        });
        movieAdapter.setOnReachEndListenerFirstPage(new MovieAdapter.OnReachEndListenerFirstPage() {
            @Override
            public void onReachEndFirstPage() {
                actionButton.setVisibility(FloatingActionButton.VISIBLE);
            }
            @Override
            public void onReachBeginFirstPage() {
                actionButton.setVisibility(FloatingActionButton.INVISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.itemMain:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.itemFavourite:
                Intent favouriteIntent = new Intent(this, FavouriteActivity.class);
                startActivity(favouriteIntent);
                break;
            case R.id.itemBookmark:
                Intent bookmarkIntent = new Intent(this, BookmarkActivity.class);
                startActivity(bookmarkIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
