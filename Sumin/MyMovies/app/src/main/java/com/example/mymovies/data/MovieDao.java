package com.example.mymovies.data;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@androidx.room.Dao
public interface MovieDao {
    @Query("SELECT * FROM movies")
    LiveData<List<Movie>> getAllMovies();

    @Query("SELECT * FROM favourite_movies")
    LiveData<List<FavouriteMovie>> getAllFavouriteMovies();

    @Query("SELECT * FROM bookmark_movies")
    LiveData<List<BookmarkMovie>> getAllBookmarkMovies();

    @Query("SELECT * FROM movies WHERE id == :movieId")
    Movie getMovieById(int movieId);

    @Query("SELECT * FROM favourite_movies WHERE id == :favouriteMovieId")
    FavouriteMovie getFavouriteMovieById(int favouriteMovieId);

    @Query("SELECT * FROM bookmark_movies WHERE id == :bookmarkMovieId")
    BookmarkMovie getBookmarkMovieById(int bookmarkMovieId);

    @Query("DELETE FROM movies")
    void deleteAllMovies();

    @Insert
    void insertMovie(Movie movie);

    @Delete
    void deleteMovie(Movie movie);

    @Insert
    void insertFavouriteMovie(FavouriteMovie movie);

    @Delete
    int deleteFavouriteMovie(FavouriteMovie movie);

    @Insert
    void insertBookmarkMovie(BookmarkMovie movie);

    @Delete
    int deleteBookmarkMovie(BookmarkMovie movie);

    @Query("DELETE FROM favourite_movies WHERE id = :id")
    void deleteFavouriteMovieById(int id);

    @Query("DELETE FROM bookmark_movies WHERE id = :id")
    void deleteBookmarkMovieById(int id);
}
