package com.example.mymovies;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymovies.adapters.MovieAdapter;
import com.example.mymovies.data.BookmarkMovie;
import com.example.mymovies.data.FavouriteMovie;
import com.example.mymovies.data.MainViewModel;
import com.example.mymovies.data.Movie;
import com.example.mymovies.utils.JSONUtils;
import com.example.mymovies.utils.NetworkUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<JSONObject> {
    Switch switchSort;
    TextView textViewPopularity;
    TextView textViewTopRated;
    RecyclerView recyclerViewPoster;
    MovieAdapter movieAdapter;
    MainViewModel viewModel;
    ProgressBar progressBarLoading;
    FloatingActionButton actionButton;
    HashMap<Integer, ImageView> imageViewAddToFavourite = new HashMap<>();
    HashMap<Integer, ImageView> imageViewAddToBookmark = new HashMap<>();

    private static final int LOADER_ID = 111;
    private static boolean isLoading = false;
    private LoaderManager loaderManager;
    private static int methodOfSort;

    public static String lang;

    private static int page = 1;

    private FavouriteMovie favouriteMovie;
    private BookmarkMovie bookmarkMovie;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchSort = findViewById(R.id.switchSort);
        textViewPopularity = findViewById(R.id.textViewPopularity);
        textViewTopRated = findViewById(R.id.textViewTopRated);
        recyclerViewPoster = findViewById(R.id.recyclerViewPoster);
        progressBarLoading = findViewById(R.id.progressBarLoading);
        actionButton = findViewById(R.id.floatingActionButton);
        lang = Locale.getDefault().getLanguage();
        loaderManager = LoaderManager.getInstance(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(R.string.nameApplication);
        viewModel = new ViewModelProvider(new ViewModelStore(), new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(MainViewModel.class);
        //viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        movieAdapter = new MovieAdapter(this);
        recyclerViewPoster.setLayoutManager(new GridLayoutManager(this, getColumnCount()));
        switchSort.setChecked(true);
        recyclerViewPoster.setAdapter(movieAdapter);
        switchSort.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                page = 1;                       //при переключении выставляем загрузку с первой страницы
                setMethodOfSort(isChecked);
            }
        });
        switchSort.setChecked(false);
        movieAdapter.setOnPosterClickListener(new MovieAdapter.OnPosterClickListener() {
            @Override
            public void onPosterClick(int position) {
                Movie movie = movieAdapter.getMovies().get(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("id", movie.getId());
                intent.putExtra("nameActivity", "MainActivity");
                startActivity(intent);
            }
        });
        movieAdapter.setOnReachEndListener(new MovieAdapter.OnReachEndListener() {
            @Override
            public void onReachEnd() {
                if (!isLoading) {
                    downloadData(methodOfSort, page);
                }
            }
        });
        movieAdapter.setOnReachEndListenerFirstPage(new MovieAdapter.OnReachEndListenerFirstPage() {
            @Override
            public void onReachEndFirstPage() {
                actionButton.setVisibility(FloatingActionButton.VISIBLE);
            }
            @Override
            public void onReachBeginFirstPage() {
                actionButton.setVisibility(FloatingActionButton.INVISIBLE);
            }
        });
        movieAdapter.setOnLikeClickListener(new MovieAdapter.OnLikeClickListener() {
            @Override
            public void onLikeClick(int position) {
                onClickChangeFavouriteMain(position);
             }
        });
        movieAdapter.setOnBookmarkClickListener(new MovieAdapter.OnBookmarkClickListener() {
            @Override
            public void onBookmarkClick(int position) {
                onClickChangeBookmarkMain(position);
            }
        });
        LiveData<List<Movie>> moviesFromLiveData = viewModel.getMovies();
        //если происходят изменения в БД, то объект LiveData
        //вызовет метод у onCnanged у наблюдателя, который на него подписался
        moviesFromLiveData.observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                //если данные изменились в БД, то изменится и List<Movie>, а следовательно вызовется метод onChanged
                if (page == 1) {
                    movieAdapter.setMovies(movies);
                }
            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewPoster.smoothScrollToPosition(0);
            }
        });
    }

    private int getColumnCount(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int witdh = (int) (displayMetrics.widthPixels / displayMetrics.density);
        return witdh / 185 > 2 ? witdh / 185 : 2;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Менюшка
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.itemMain:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.itemFavourite:
                Intent favouriteIntent = new Intent(this, FavouriteActivity.class);
                startActivity(favouriteIntent);
                break;
            case R.id.itemBookmark:
                Intent bookmarkIntent = new Intent(this, BookmarkActivity.class);
                startActivity(bookmarkIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickSetPopularity(View view) {
        setMethodOfSort(false);
        switchSort.setChecked(false);
        clearHashMapsImageView();
    }

    public void onClickSetTopRated(View view) {
        setMethodOfSort(true);
        switchSort.setChecked(true);
        clearHashMapsImageView();
    }

    private void clearHashMapsImageView(){
        imageViewAddToFavourite.clear();
        imageViewAddToBookmark.clear();
    }

    private void setMethodOfSort(boolean isTopRated){
        if (isTopRated) {
            methodOfSort = NetworkUtils.TOP_RATED;
            textViewPopularity.setTextColor(getResources().getColor(R.color.white_color));
            textViewTopRated.setTextColor(getResources().getColor(R.color.colorAccent));
            clearHashMapsImageView();
        }
        else {
            methodOfSort = NetworkUtils.POPULARITY;
            textViewPopularity.setTextColor(getResources().getColor(R.color.colorAccent));
            textViewTopRated.setTextColor(getResources().getColor(R.color.white_color));
            clearHashMapsImageView();
        }
        actionButton.setVisibility(FloatingActionButton.INVISIBLE);
        downloadData(methodOfSort, page);
    }

    private void downloadData(int methodOfSort, int page){
//        JSONObject jsonObject = NetworkUtils.getJSONFromNetwork(methodOfSort, page);
//        List<Movie> movies = JSONUtils.getMoviesFromJSON(jsonObject);
//        if (movies != null && !movies.isEmpty()){
//            viewModel.deleteAllMovies();
//            for (Movie movie: movies){
//                viewModel.insertMovie(movie);
//            }
//        }
        URL url = NetworkUtils.buildURL(methodOfSort, page, lang);
        Bundle bundle = new Bundle();
        bundle.putString("url", url.toString());
        loaderManager.restartLoader(LOADER_ID, bundle, this);
    }


    //метод создания загрузчика данных
    @NonNull
    @Override
    public Loader<JSONObject> onCreateLoader(int id, @Nullable Bundle args) {
        NetworkUtils.JSONLoader jsonLoader = new NetworkUtils.JSONLoader(this, args);
        jsonLoader.setOnStartLoadingListener(new NetworkUtils.JSONLoader.OnStartLoadingListener() {
            @Override
            public void onStartLoading() {
                isLoading = true;                                           //при старте загрузки данных выставим флаг загрузки
                progressBarLoading.setVisibility(ProgressBar.VISIBLE);      //покажем загрузку элементом загрузки данных progressBarLoading
            }
        });
        return jsonLoader;                                                  //вернем ссылку на загрузчик, который имеет постоянный айди
    }

    //данный метод запускается полной загрузки данных, которые сохраняются в объекте JSONObject
    @Override
    public void onLoadFinished(@NonNull Loader<JSONObject> loader, JSONObject jsonObject) {
        List<Movie> movies = JSONUtils.getMoviesFromJSON(jsonObject);       //распарсим сохраненные данные в массив фильмов
        if (movies != null && !movies.isEmpty()){
            //если данные загружены, то удаляем все данные из БД и адаптера, если данные загружены для 1-й страницы
            if (page == 1) {
                viewModel.deleteAllMovies();
                movieAdapter.clear();
            }
            //добавляем новые загруженные данные в БД
            for (Movie movie: movies){
                viewModel.insertMovie(movie);
            }
            movieAdapter.addMovies(movies);                         //обновляем recyclerView
            page++;                                                 //увеличиваем номер страницы
        }
        isLoading = false;                  //сбрасываем флаг загрузки
        progressBarLoading.setVisibility(ProgressBar.INVISIBLE);    //прячем элемент загрузки
        loaderManager.destroyLoader(LOADER_ID);                     //уничтожаем загрузчик
    }

    @Override
    public void onLoaderReset(@NonNull Loader<JSONObject> loader) {
    }

    public void onClickChangeFavouriteMain(int position) {
        setFavouriteMovie(position, imageViewAddToFavourite.get(position));
        if (favouriteMovie == null) {
            favouriteMovie = new FavouriteMovie(movie);
            viewModel.insertFavouriteMovie(favouriteMovie);
            Toast.makeText(MainActivity.this, R.string.add_to_favourite, Toast.LENGTH_SHORT).show();
        }else{
            viewModel.deleteFavouriteMovieById(favouriteMovie.getId());
            Toast.makeText(MainActivity.this, R.string.remove_from_favourite, Toast.LENGTH_SHORT).show();
            favouriteMovie = null;
        }
        setImageFavouriteMovie(position);
    }

    public void setImageFavouriteMovie(int position){
        if (favouriteMovie == null){
            imageViewAddToFavourite.get(position).setImageResource(R.drawable.like_favorite_heart);
        }else{
            imageViewAddToFavourite.get(position).setImageResource(R.drawable.like_favorite_heart_remove);
        }
    }

    public void setFavouriteMovie(int position, ImageView imageViewLike){
        movie = movieAdapter.getMovies().get(position);
        favouriteMovie = viewModel.getFavouriteMovieById(movie.getId());
        if (imageViewAddToFavourite.get(position) == null) {
            imageViewAddToFavourite.put(position, imageViewLike);
        }
        setImageFavouriteMovie(position);
    }

    public void onClickChangeBookmarkMain(int position) {
        setBookmarkMovie(position, imageViewAddToBookmark.get(position));
        if (bookmarkMovie == null) {
            bookmarkMovie = new BookmarkMovie(movie);
            viewModel.insertBookmarkMovie(bookmarkMovie);
            Toast.makeText(MainActivity.this, R.string.add_to_bookmark, Toast.LENGTH_SHORT).show();
        }else{
            viewModel.deleteBookmarkMovieById(bookmarkMovie.getId());
            Toast.makeText(MainActivity.this, R.string.remove_from_bookmark, Toast.LENGTH_SHORT).show();
            bookmarkMovie = null;
        }
        setImageBookmarkMovie(position);
    }

    public void setBookmarkMovie(int position, ImageView imageViewBookmark){
        movie = movieAdapter.getMovies().get(position);
        bookmarkMovie = viewModel.getBookmarkMovieById(movie.getId());
        if (imageViewAddToBookmark.get(position) == null) {
            imageViewAddToBookmark.put(position, imageViewBookmark);
        }
        setImageBookmarkMovie(position);
    }

    public void setImageBookmarkMovie(int position){
        if (bookmarkMovie == null){
            imageViewAddToBookmark.get(position).setImageResource(R.drawable.bookmark_white_icon);
        }else{
            imageViewAddToBookmark.get(position).setImageResource(R.drawable.bookmark_black_icon);
        }
    }
}