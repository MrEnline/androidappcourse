package com.example.mymovies.service;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import androidx.core.view.MotionEventCompat;

public abstract class OnSwipeTouchListener implements OnTouchListener {

    private final GestureDetector gestureDetector;
    private View view;
    private GestureListener OnGestureListener;
    private boolean onDownFlag = true;

    public OnSwipeTouchListener (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        view = v;
        return gestureDetector.onTouchEvent(event);
    }

    public void setOnDownFlag(boolean onDownFlag) {
        this.onDownFlag = onDownFlag;
    }

    private class GestureListener extends SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD_X = -1;
        private static final int SWIPE_VELOCITY_THRESHOLD_X = 0;
        private static final int SWIPE_THRESHOLD_Y = 50;
        private static final int SWIPE_VELOCITY_THRESHOLD_Y = 500;
        private boolean result = false;

        @Override
        public boolean onDown(MotionEvent e) {
            return onDownFlag;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            onDownView();
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.i("onFling", "onFling");
            try {
                //float diffY = e2.getY() - e1.getY();
                //float diffX = e2.getX() - e1.getX();
                Log.i("velocityX", String.valueOf(velocityX));
                if (Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD_X){
//                  Log.i("velocityY", String.valueOf(velocityY));
                    if (velocityX > SWIPE_VELOCITY_THRESHOLD_X) {
                        onSwipeRight();
                    } else if (velocityX < SWIPE_VELOCITY_THRESHOLD_X){
                        onSwipeLeft();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public abstract void onSwipeRight();
    public abstract void onSwipeLeft();
    public abstract void onDownView();
    //public abstract void onSwipeBottom(View view);
    //public abstract void onSwipeTop(View view);
    //public abstract void onClick(View view);
    //public abstract boolean onLongClick(View view);

//    public void onSwipeRight() {
//    }
//
//    public void onSwipeLeft() {
//    }

//    public void onSwipeTop() {
//    }
//
//    public void onSwipeBottom() {
//    }
}


