package com.example.mymovies.data;

import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "bookmark_movies")
public class BookmarkMovie extends Movie {
        public BookmarkMovie(int uniqeId, int id, int voteCount, String title, String originalTitle, String overview,
                              String posterPath, String bigPosterPath, String backdropPath, double voteAverage, String releaseDate) {
            super(id, voteCount, title, originalTitle, overview, posterPath, bigPosterPath, backdropPath, voteAverage, releaseDate);
        }

        @Ignore
        public BookmarkMovie(Movie movie) {
            super(movie.getUniqeId(), movie.getId(), movie.getVoteCount(), movie.getTitle(), movie.getOriginalTitle(), movie.getOverview(),
                    movie.getPosterPath(), movie.getBigPosterPath(), movie.getBackdropPath(), movie.getVoteAverage(), movie.getReleaseDate());
        }
}
