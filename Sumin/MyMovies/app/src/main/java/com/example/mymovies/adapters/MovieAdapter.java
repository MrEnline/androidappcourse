package com.example.mymovies.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymovies.MainActivity;
import com.example.mymovies.R;
import com.example.mymovies.data.Movie;
import com.example.mymovies.service.OnSwipeTouchListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private static final float limdX  = -170;
    private static final float dXStart = 0.0f;

    private boolean isSwipeView = true;

    List<Movie> movies;
    OnPosterClickListener onPosterClickListener;
    OnReachEndListener onReachEndListener;
    OnReachEndListenerFirstPage onReachEndListenerFirstPage;
    OnSwipeTouchListener onSwipeTouchListener;
    OnLikeClickListener onLikeClickListener;
    OnBookmarkClickListener onBookmarkClickListener;
    MainActivity mainActivity;

    public MovieAdapter(){}

    public MovieAdapter(boolean isSwipeView){
        this.isSwipeView = isSwipeView;
    }

    public MovieAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public interface OnPosterClickListener{
        void onPosterClick(int position);
    }

    public interface OnReachEndListener{
        void onReachEnd();
    }

    public interface OnReachEndListenerFirstPage{
        void onReachEndFirstPage();
        void onReachBeginFirstPage();
    }

    public interface OnLikeClickListener{
        void onLikeClick(int position);
    }

    public interface OnBookmarkClickListener{
        void onBookmarkClick(int position);
    }

    public void setOnPosterClickListener(OnPosterClickListener onPosterClickListener) {
        this.onPosterClickListener = onPosterClickListener;
    }

    public void setOnReachEndListener(OnReachEndListener onReachEndListener) {
        this.onReachEndListener = onReachEndListener;
    }

    public void setOnReachEndListenerFirstPage(OnReachEndListenerFirstPage onReachEndListenerFirstPage) {
        this.onReachEndListenerFirstPage = onReachEndListenerFirstPage;
    }

    public void setOnSwipeTouchListener(OnSwipeTouchListener onSwipeTouchListener) {
        this.onSwipeTouchListener = onSwipeTouchListener;
    }

    public void setOnLikeClickListener(OnLikeClickListener onLikeClickListener) {
        this.onLikeClickListener = onLikeClickListener;
    }

    public void setOnBookmarkClickListener(OnBookmarkClickListener onBookmarkClickListener) {
        this.onBookmarkClickListener = onBookmarkClickListener;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        if (movies.size() >= 20 && position > movies.size() - 6 && onReachEndListener != null){
            onReachEndListener.onReachEnd();
        }
        if (position > 8 && onReachEndListenerFirstPage != null){
            onReachEndListenerFirstPage.onReachEndFirstPage();
        }
        if (position == 0 && onReachEndListenerFirstPage != null){
            onReachEndListenerFirstPage.onReachBeginFirstPage();
        }
        if (holder.imageViewSmallPoster.getTranslationX() != 0.0f){
            holder.setStartPositionView();
        }
        Movie movie = movies.get(position);
        Picasso.get().load(movie.getPosterPath()).into(holder.imageViewSmallPoster);
    }

    @Override
    public int getItemCount() {
        if (movies != null)
            return movies.size();
        return -1;
    }

    public void clear(){
        movies.clear();
        notifyDataSetChanged(); //уведомляет текущий адаптер о том, что данные изменились
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovies(List<Movie> movies){
        if(this.movies != null){
            this.movies.addAll(movies);
            notifyDataSetChanged();
        }
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewSmallPoster;
        private ImageView imageViewBookmarkFilm1;
        private ImageView imageViewBookmarkFilm2;
        private ImageView imageViewLike1;
        private ImageView imageViewLike2;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewSmallPoster = itemView.findViewById(R.id.imageViewSmallPoster);
            imageViewLike1 = itemView.findViewById(R.id.imageViewLike1);
            imageViewLike2 = itemView.findViewById(R.id.imageViewLike2);
            imageViewBookmarkFilm1 = itemView.findViewById(R.id.imageViewBookmarkFilm1);
            imageViewBookmarkFilm2 = itemView.findViewById(R.id.imageViewBookmarkFilm2);
            itemView.setOnTouchListener(new OnSwipeTouchListener(itemView.getContext()) {
                @Override
                public void onSwipeRight() {
                    int position = getAdapterPosition();
                    if (isSwipeView) {
                        if (position % 2 == 0) {
                            if (imageViewSmallPoster.getTranslationX() == limdX) {
                                setPositionGroupView(dXStart, imageViewSmallPoster, imageViewBookmarkFilm2, imageViewLike2, View.INVISIBLE);
                            }
                        } else {
                            if (imageViewSmallPoster.getTranslationX() == dXStart) {
                                setPositionGroupView(Math.abs(limdX), imageViewSmallPoster, imageViewBookmarkFilm1, imageViewLike1, View.VISIBLE);
                                mainActivity.setFavouriteMovie(position, imageViewLike1);
                                mainActivity.setBookmarkMovie(position, imageViewBookmarkFilm1);
                            }
                        }
                    }
                }

                @Override
                public void onSwipeLeft() {
                    int position = getAdapterPosition();
                    if (isSwipeView) {
                        if (position % 2 == 0) {
                            if (imageViewSmallPoster.getTranslationX() == dXStart) {
                                setPositionGroupView(limdX, imageViewSmallPoster, imageViewBookmarkFilm2, imageViewLike2, View.VISIBLE);
                                mainActivity.setFavouriteMovie(position, imageViewLike2);
                                mainActivity.setBookmarkMovie(position, imageViewBookmarkFilm2);
                            }
                        } else {
                            if (imageViewSmallPoster.getTranslationX() == Math.abs(limdX)) {
                                setPositionGroupView(dXStart, imageViewSmallPoster, imageViewBookmarkFilm1, imageViewLike1, View.INVISIBLE);
                            }
                        }
                    }
                }

                @Override
                public void onDownView() {
                    if (onPosterClickListener != null) {
                        onPosterClickListener.onPosterClick(getAdapterPosition());
                    }
                }
            });

            imageViewLike1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLikeClickListener != null){
                        onLikeClickListener.onLikeClick(getAdapterPosition());
                        setStartPositionView();
                    }
                }
            });

            imageViewLike2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLikeClickListener != null){
                        onLikeClickListener.onLikeClick(getAdapterPosition());
                        setStartPositionView();
                    }
                }
            });

            imageViewBookmarkFilm1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onBookmarkClickListener != null){
                        onBookmarkClickListener.onBookmarkClick(getAdapterPosition());
                        setStartPositionView();
                    }
                }
            });

            imageViewBookmarkFilm2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onBookmarkClickListener != null){
                        onBookmarkClickListener.onBookmarkClick(getAdapterPosition());
                        setStartPositionView();
                    }
                }
            });
       }

        public void setStartPositionView(){
            imageViewSmallPoster.setTranslationX(0.0f);
            imageViewBookmarkFilm1.setVisibility(View.INVISIBLE);
            imageViewBookmarkFilm2.setVisibility(View.INVISIBLE);
            imageViewLike1.setVisibility(View.INVISIBLE);
            imageViewLike2.setVisibility(View.INVISIBLE);
        }

        public void setPositionGroupView(float dX, ImageView imageViewSmallPosterCurrent, ImageView imageViewBookmarkFilmCurrent, ImageView imageViewLikeCurrent, int visible){
            imageViewSmallPosterCurrent.setTranslationX(dX);
            imageViewBookmarkFilmCurrent.setVisibility(visible);
            imageViewLikeCurrent.setVisibility(visible);
        }
    }
}
