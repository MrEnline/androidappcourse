package com.example.testemploees.api;

import com.example.testemploees.pojo.EmploeeResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {
    @GET("testTask.json")
    Observable<EmploeeResponse> getEmploees();
}
