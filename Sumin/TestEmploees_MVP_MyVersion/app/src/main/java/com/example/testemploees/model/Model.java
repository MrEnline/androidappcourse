package com.example.testemploees.model;

import com.example.testemploees.api.ApiFactory;
import com.example.testemploees.api.ApiService;
import com.example.testemploees.pojo.EmploeeResponse;
import com.example.testemploees.screens.emploees.EmploeeListView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class Model {
    Disposable disposable;
    CompositeDisposable compositeDisposable;

    public void loadData(EmploeeListView view){
        ApiFactory apiFactory = ApiFactory.getApiFactory();
        ApiService apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();
        disposable = apiService.getEmploees()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<EmploeeResponse>() {
                    @Override
                    public void accept(EmploeeResponse emploeeResponse) throws Exception {
                        view.showData(emploeeResponse.getResponse());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError();
                    }
                });
        compositeDisposable.add(disposable);
    }

    public void disposeDisposable(){
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }
}
