package com.example.testemploees.screens.emploees;

import android.util.Log;
import android.widget.Toast;

import com.example.testemploees.api.ApiFactory;
import com.example.testemploees.api.ApiService;
import com.example.testemploees.model.Model;
import com.example.testemploees.pojo.EmploeeResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class EmploeeListPresenter {

    EmploeeListView view;
    Model model;

    public EmploeeListPresenter(EmploeeListView view) {
        this.view = view;
        model = new Model();
    }

    public void loadData(){
        model.loadData(view);
    }

    public void disposeDisposable(){
        model.disposeDisposable();
    }
}
