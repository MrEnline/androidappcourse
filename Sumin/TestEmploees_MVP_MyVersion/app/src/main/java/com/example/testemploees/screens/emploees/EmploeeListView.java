package com.example.testemploees.screens.emploees;

import com.example.testemploees.pojo.Emploee;

import java.util.List;

public interface EmploeeListView {
    void showData(List<Emploee> emploeeList);
    void showError();
}
