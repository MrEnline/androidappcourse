package com.example.cafeorder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class OrderDetailActivity extends AppCompatActivity {

    private TextView textViewDetailsOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        textViewDetailsOrder = findViewById(R.id.textViewDetailsOrder);
        Intent intent = getIntent();
        if (intent.hasExtra("order")){
            textViewDetailsOrder.setText(intent.getStringExtra("order"));
        }else{
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}