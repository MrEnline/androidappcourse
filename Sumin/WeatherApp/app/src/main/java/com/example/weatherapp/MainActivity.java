package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.BreakIterator;
import java.text.Format;

public class MainActivity extends AppCompatActivity {

    EditText editTextCity;
    private TextView textViewWeather;
    private  final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=8c5eb87f05b913351355a302ab0b5a97&units=metric&lang=ru";
    //private  final String WEATHER_URL = "http://api.themoviedb.org/3/discover/movie?api_key=822d44a396057cfb9911de344ed2581d&language=ru-RU&sort_by=popularity.desc&page=2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextCity = findViewById(R.id.editTextCity);
        textViewWeather = findViewById(R.id.textViewWeather);
    }

    public void onClickGetWeather(View view) {
        String city = editTextCity.getText().toString().trim();
        DownloadJSONTask task = new DownloadJSONTask();
        if (!city.isEmpty())
            task.execute(String.format(WEATHER_URL, city));
    }

    private class DownloadJSONTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();
            try {
                url = new URL(strings[0]);
                Log.i("URL", strings[0]);
                urlConnection = (HttpURLConnection)url.openConnection();
                Log.i("URL", "Test0");
                InputStream stream = urlConnection.getInputStream();
                Log.i("URL", "Test1");
                InputStreamReader streamReader = new InputStreamReader(stream);
                Log.i("URL", "Test2");
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                Log.i("URL", "Test3");
                String line = bufferedReader.readLine();
                Log.i("URL", "Test4");
                while (line != null){
                    result.append(line);
                    line = bufferedReader.readLine();
                }
                Log.i("URL", "Test5");
                Log.i("URL", result.toString());
                return  result.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                String city = jsonObject.getString("name");
                String temp = jsonObject.getJSONObject("main").getString("temp");
                String description = jsonObject.getJSONArray("weather").getJSONObject(0).getString("description");
                String  weather = String.format("%s\nТемпература: %s\nНа улице: %s", city, temp,description);
                textViewWeather.setText(weather);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}