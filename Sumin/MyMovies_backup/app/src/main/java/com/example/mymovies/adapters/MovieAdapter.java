package com.example.mymovies.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymovies.MainActivity;
import com.example.mymovies.R;
import com.example.mymovies.data.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    List<Movie> movies;
    private OnPosterClickListener onPosterClickListener;
    private OnReachEndListener onReachEndListener;
    OnReachEndListenerFirstPage onReachEndListenerFirstPage;

    public MovieAdapter() {

    }

    public interface OnPosterClickListener{
        void onPosterClick(int position);
    }

    public interface OnReachEndListener{
        void onReachEnd();
    }

    public interface OnReachEndListenerFirstPage{
        void onReachEndFirstPage();
        void onReachBeginFirstPage();
    }

    public void setOnPosterClickListener(OnPosterClickListener onPosterClickListener) {
        this.onPosterClickListener = onPosterClickListener;
    }

    public void setOnReachEndListener(OnReachEndListener onReachEndListener) {
        this.onReachEndListener = onReachEndListener;
    }

    public void setOnReachEndListenerFirstPage(OnReachEndListenerFirstPage onReachEndListenerFirstPage) {
        this.onReachEndListenerFirstPage = onReachEndListenerFirstPage;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        if (movies.size() >= 20 && position > movies.size() - 6 && onReachEndListener != null){
            onReachEndListener.onReachEnd();
        }
        if (position > 8 && onReachEndListenerFirstPage != null){
            onReachEndListenerFirstPage.onReachEndFirstPage();
        }
        if (position == 0 && onReachEndListenerFirstPage != null){
            onReachEndListenerFirstPage.onReachBeginFirstPage();
        }
        //Log.i("pos", String.valueOf(position));
        Movie movie = movies.get(position);
        Picasso.get().load(movie.getPosterPath()).into(holder.imageViewSmallPoster);
    }

    @Override
    public int getItemCount() {
        if (movies != null)
            return movies.size();
        return -1;
    }

    public void clear(){
        movies.clear();
        notifyDataSetChanged(); //уведомляет текущий адаптер о том, что данные изменились
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovies(List<Movie> movies){
        if(this.movies != null){
            this.movies.addAll(movies);
            notifyDataSetChanged();
        }
    }



    class MovieViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewSmallPoster;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewSmallPoster = itemView.findViewById(R.id.imageViewSmallPoster);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onPosterClickListener != null){
                        onPosterClickListener.onPosterClick(getAdapterPosition());
                    }
                }
            });
        }

    }
}
