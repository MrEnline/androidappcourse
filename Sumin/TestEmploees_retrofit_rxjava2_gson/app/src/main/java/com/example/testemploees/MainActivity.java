package com.example.testemploees;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.testemploees.adapters.EmploeeAdapter;
import com.example.testemploees.api.ApiFactory;
import com.example.testemploees.api.ApiService;
import com.example.testemploees.pojo.Emploee;
import com.example.testemploees.pojo.EmploeeResponse;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    EmploeeAdapter emploeeAdapter;
    List<Emploee> emploees;
    Disposable disposable;
    CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerViewEmploee);
        emploees = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        emploeeAdapter = new EmploeeAdapter();
        emploeeAdapter.setEmploees(emploees);
        recyclerView.setAdapter(emploeeAdapter);
        ApiFactory apiFactory = ApiFactory.getApiFactory();
        ApiService apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();
        disposable = apiService.getEmploees().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<EmploeeResponse>() {
                    @Override
                    public void accept(EmploeeResponse emploeeResponse) throws Exception {
                        Log.i("qwerty", emploeeResponse.getResponse().get(5).toString());
                        emploeeAdapter.setEmploees(emploeeResponse.getResponse());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }
}