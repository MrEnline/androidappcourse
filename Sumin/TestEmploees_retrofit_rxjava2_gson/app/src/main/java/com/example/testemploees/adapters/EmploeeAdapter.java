package com.example.testemploees.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testemploees.R;
import com.example.testemploees.pojo.Emploee;

import java.util.List;

public class EmploeeAdapter extends RecyclerView.Adapter<EmploeeAdapter.EmploeeViewHolder> {

    List<Emploee> emploees;

    @NonNull
    @Override
    public EmploeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //создаем view на основе макета
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.emploee_item, parent, false);
        return new EmploeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmploeeViewHolder holder, int position) {
        holder.textViewName.setText(emploees.get(position).getfName());
        holder.textViewLastName.setText(emploees.get(position).getlName());
    }

    @Override
    public int getItemCount() {
        return emploees.size();
    }

    public List<Emploee> getEmploees() {
        return emploees;
    }

    public void setEmploees(List<Emploee> emploees) {
        this.emploees = emploees;
        notifyDataSetChanged();
    }

    //определяем какие view-шки на макете emploee_item будем заполнять
    public class EmploeeViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewName;
        private TextView textViewLastName;

        public EmploeeViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewLastName = itemView.findViewById(R.id.textViewLastName);
        }
    }
}
