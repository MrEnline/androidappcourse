package com.example.toolsshop;

public class Drill {
    private String title;
    private String info;
    private int ImageResourceId;

    public Drill(String title, String info, int imageResourceId) {
        this.title = title;
        this.info = info;
        ImageResourceId = imageResourceId;
    }

    public String getTitle() {
        return title;
    }

    public String getInfo() {
        return info;
    }

    public int getImageResourceId() {
        return ImageResourceId;
    }

    @Override
    public String toString() {
        return title;
    }
}
